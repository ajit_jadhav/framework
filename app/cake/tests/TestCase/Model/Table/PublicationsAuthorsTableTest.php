<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PublicationsAuthorsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PublicationsAuthorsTable Test Case
 */
class PublicationsAuthorsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PublicationsAuthorsTable
     */
    public $PublicationsAuthors;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.publications_authors',
        'app.publications',
        'app.authors'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PublicationsAuthors') ? [] : ['className' => PublicationsAuthorsTable::class];
        $this->PublicationsAuthors = TableRegistry::getTableLocator()->get('PublicationsAuthors', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PublicationsAuthors);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
