<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Inscription Entity
 *
 * @property int $id
 * @property int|null $artifact_id
 * @property string|null $transliteration
 * @property string|null $transliteration_clean
 * @property string|null $tranliteration_sign_names
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime|null $created
 * @property int|null $credit_to
 * @property bool|null $is_latest
 * @property string|null $annotation
 * @property bool|null $atf2conll_diff_resolved
 * @property bool|null $atf2conll_diff_unresolved
 * @property string|null $comments
 * @property string|null $structure
 * @property string|null $translations
 * @property string|null $transcriptions
 *
 * @property \App\Model\Entity\Artifact $artifact
 */
class Inscription extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'transliteration' => true,
        'transliteration_clean' => true,
        'tranliteration_sign_names' => true,
        'created_by' => true,
        'created' => true,
        'credit_to' => true,
        'is_latest' => true,
        'annotation' => true,
        'atf2conll_diff_resolved' => true,
        'atf2conll_diff_unresolved' => true,
        'comments' => true,
        'structure' => true,
        'translations' => true,
        'transcriptions' => true,
        'artifact' => true
    ];
}
