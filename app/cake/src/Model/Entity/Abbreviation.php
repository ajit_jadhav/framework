<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Abbreviation Entity
 *
 * @property int $id
 * @property string $abbreviation
 * @property string|null $fullform
 * @property string|null $type
 * @property int|null $publication_id
 *
 * @property \App\Model\Entity\Publication[] $publications
 */
class Abbreviation extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'abbreviation' => true,
        'fullform' => true,
        'type' => true,
        'publication_id' => true,
        'publications' => true,
    ];
}
