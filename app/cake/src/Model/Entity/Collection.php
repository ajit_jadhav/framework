<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Collection Entity
 *
 * @property int $id
 * @property string $collection
 * @property string|null $geo_coordinates
 * @property int|null $slug
 * @property bool|null $is_private
 *
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class Collection extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'collection' => true,
        'geo_coordinates' => true,
        'slug' => true,
        'is_private' => true,
        'artifacts' => true
    ];
}
