<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class EditorsPublication extends Entity
{
    protected $_accessible = [
        'publication_id' => true,
        'editor_id' => true,
        'publication' => true,
        'author' => true,
        'sequence' => true
    ];
}
