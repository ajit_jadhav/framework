<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArtifactsCredit Entity
 *
 * @property int $id
 * @property int $artifact_id
 * @property int $credit_id
 * @property \Cake\I18n\FrozenTime $date
 * @property string $credit_type
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\Credit $credit
 */
class ArtifactsCredit extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'credit_id' => true,
        'date' => true,
        'credit_type' => true,
        'artifact' => true,
        'credit' => true
    ];
}
