<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class AgadeMailsTable extends Table
{
    public function initialize(array $config)
    {
        $this->belongsToMany('CdliTags', [
            'joinTable' => 'agade_mails_cdli_tags',
        ]);
    }
}
