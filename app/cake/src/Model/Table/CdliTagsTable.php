<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class CdliTagsTable extends Table
{
    public function initialize(array $config)
    {
        $this->belongsToMany('AgadeMails', [
            'joinTable' => 'agade_mails_cdli_tags',
        ]);
    }
}
