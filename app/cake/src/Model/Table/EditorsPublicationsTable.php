<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class EditorsPublicationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('editors_publications');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Publications', [
            'foreignKey' => 'publication_id'
        ]);
        $this->belongsTo('Authors', [
            'foreignKey' => 'editor_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('editor_id')
            ->nonNegativeInteger('editor_id');

        $validator
            ->scalar('publication_id')
            ->nonNegativeInteger('publication_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['publication_id'], 'Publications'));
        $rules->add($rules->existsIn(['editor_id'], 'Authors', 'This editor does not exist'));
        $rules->add($rules->isUnique(
            ['editor_id', 'publication_id'],
            'This link already exists'
        ));

        $rules->add($rules->isUnique(
            ['publication_id', 'editor_id'],
            'This link already exists'
        ));
        
        return $rules;
    }
}
