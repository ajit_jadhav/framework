<?php
namespace App\View;

use Cake\Event\EventManager;
use Cake\Http\Response;
use Cake\Http\ServerRequest as Request;
use CsvView\View\CsvView;

class ArtifactTableView extends CsvView
{

    /**
     * Which delimiter and Content-Type to use ('csv', 'xlsx', 'tsv').
     *
     * @var string
     */
    protected $tableType;

    /**
     * Constructor
     *
     * @param \Cake\Http\ServerRequest|null $request      Request instance.
     * @param \Cake\Http\Response|null      $response     Response instance.
     * @param \Cake\Event\EventManager|null $eventManager EventManager instance.
     * @param array                         $viewOptions  An array of view options
     */
    public function __construct(
        Request $request = null,
        Response $response = null,
        EventManager $eventManager = null,
        array $viewOptions = []
    ) {
        parent::__construct($request, $response, $eventManager, $viewOptions);

        $this->tableType = $this->viewVars['_tableType'];
        $this->response = $this->response->withType($this->tableType);
    }

    protected function _serialize()
    {
        $artifact = $this->viewVars['artifact'];

        $artifact_basic = [
            ['id','ark_no','primary_publication_comments','cdli_collation','composite_no','condition_description','created','Date Comments','Modified','Designation','Electronic Publication','Elevation','Excavation No','Findspot Square','Join Information','Lineart Up','Museum No','Artifact Preservation','Photo Up','Seal No','Stratigraphic Level','Surface Preservation','Provenience','Period','Artifact Type','Created By','Height','Thickness','Width','Accounting Period','Written In','Is Object Type Uncertain','Weight','Dates Referenced','Is Public','Is Atf Public','Are Images Public','Is Provenience Uncertain','Is Period Uncertain','Is School Text','Cdli Comments','Findspot Comments','Seal Information','General Comments','Accession No','Alternative Years','Dumb2','Custom Designation','Period Comments','Provenience Comments','Collection','Genre','Material','Language','Date','Month','Years'],
            [$artifact->id,$artifact->ark_no,$artifact->primary_publication_comments,$artifact->cdli_collation,$artifact->composite_no,$artifact->condition_description,$artifact->created,$artifact->date_comments,$artifact->modified,$artifact->designation,$artifact->electronic_publication,$artifact->elevation,$artifact->excavation_no,$artifact->findspot_square,$artifact->join_information,$artifact->lineart_up,$artifact->museum_no,$artifact->artifact_preservation,$artifact->photo_up,$artifact->seal_no,$artifact->stratigraphic_level,$artifact->surface_preservation,$artifact->provenience->provenience,$artifact->period->period,$artifact->artifact_type->artifact_type,$artifact->created_by,$artifact->height,$artifact->thickness,$artifact->width,$artifact->accounting_period,$artifact->written_in,$artifact->is_object_type_uncertain,$artifact->weight,$artifact->dates_referenced,$artifact->is_public ? __('Yes') : __('No'),$artifact->is_atf_public ? __('Yes') : __('No'),$artifact->are_images_public ? __('Yes') : __('No'),$artifact->is_provenience_uncertain ? __('Yes') : __('No'),$artifact->is_period_uncertain ? __('Yes') : __('No'),$artifact->is_school_text ? __('Yes') : __('No'),$artifact->cdli_comments,$artifact->findspot_comments,$artifact->seal_information,$artifact->general_comments,$artifact->accession_no,$artifact->alternative_years,$artifact->dumb2,$artifact->custom_designation,$artifact->period_comments,$artifact->provenience_comments,
                array_key_exists(0, $artifact->collections)? __($artifact->collections[0]->collection): null,
                array_key_exists(0, $artifact->genres)? __($artifact->genres[0]->genre) : null,
                array_key_exists(0, $artifact->materials)? __($artifact->materials[0]->material) : null,
                array_key_exists(0, $artifact->languages)? __($artifact->languages[0]->language) : null,
                array_key_exists(0, $artifact->dates)? __($artifact->dates[0]->day_no) : null,
                array_key_exists(0, $artifact->dates)? __($artifact->dates[0]->month->month_no) : null,
                array_key_exists(0, $artifact->dates)? __($artifact->dates[0]->year->year_no) : null,
            ]
        ];

        //create array for external resources
        $external_resources = [['External_resource','Base_url','Project_url','Abbrev','External_resource_key']];
        foreach ($artifact->external_resources as $external_resource) {
            array_push($external_resources, [$external_resource->external_resource,$external_resource->base_url,$external_resource->project_url, $external_resource->abbrev,$external_resource->_joinData->external_resource_key]);
        }

        //create array for publication
        $publications = [['Publication Year','Entry Type','Address','Annote','Book Title','Chapter','Crossref','Edition','Editor','How Published','Institution','Journal','Publication Month', 'Note','Number','Organization','Pages','Publisher','School','Title','Volume','Publication History','Abbreviation Id', 'Series', 'Oclc', 'Designation', 'Publication Type']];
        foreach ($artifact->publications as $publication) {
            array_push($publications, [$publication->year, $publication->entry_type,$publication->address,$publication->annote,$publication->book_title,$publication->chapter,$publication->crossref,$publication->edition,$publication->editor,$publication->how_published,$publication->institution,$publication->journal,$publication->month,$publication->note,$publication->number,$publication->organization,$publication->pages,$publication->publisher,$publication->school,$publication->title,$publication->volume,$publication->publication_history,$publication->abbreviation_id,$publication->series,$publication->oclc,$publication->designation,$publication->_joinData->publication_type]);
        }

        //merge three arrays into final csv array
        //$artifact_csv = array_merge($artifact_csv, $publications,$external_resources);
        //$artifact_csv = array_merge_recursive($artifact_csv, $publications,$external_resources);
        $artifact_csv = [];
        $num = max(count($artifact_basic), count($external_resources), count($publications));
        function fillwithnull($array, $i)
        {
            return array_key_exists($i, $array) ? $array[$i]:array_fill(0, count($array[0]), null);
        }
        for ($i=0;$i<$num;$i++) {
            array_push(
                $artifact_csv,
                array_merge(
                    fillwithnull($artifact_basic, $i),
                    fillwithnull($external_resources, $i),
                    fillwithnull($publications, $i)
                )
            );
        }

        $this->set('artifact_csv', $artifact_csv);
        $this->set('_delimiter', $this->tableType === 'tsv' ? chr(9) : ',');
        $this->set('_serialize', 'artifact_csv');

        return parent::_serialize();
    }
}
