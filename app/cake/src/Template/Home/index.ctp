<?php
/*
Implemention quicknotes

1)Data of news and highlights is supposed to be an array by the names of 'highlights' and 'newsarticles'
2)Title of card will be trimmed if it's length is more than 30 characters.
3)Body of card will be trimmed if it's length is more than 100 characters.
*/

?>

<div class="container">
    <div>
        <h1 class="display-3 text-left header-txt">The CDLI Collection</h1>
        <p class="text-left home-desc mt-4">By making the form and content of cuneiform texts available online, the CDLI is opening pathways to the rich historical tradition of the ancient Middle East. In close collaboration with researchers, museums and an engaged public, the project seeks to unharness the extraordinary content of these earliest witnesses to our shared world heritage.</p>
    </div>

    <div class="mt-5">
        <?= $this->Form->create("", ['type'=>'get']) ?>
        <div class="search-area d-flex justify-content-between">
            <?= $this->Form->Text('searchQuery', ['placeholder' =>'Search for publications, provenience, collection no.','class' => 'px-3', 'aria-label'=>'search-bar']); ?>
            <?= $this->Form->button('Search', ['type' => 'submit', 'class' => 'btn cdli-btn-blue']); ?>
        </div>
        <?= $this->Form->button('Search', ['type' => 'submit', 'class' => 'd-md-none btn cdli-btn-blue w-100 mt-3']); ?>
        <?= $this->Form->end()?>
    </div>
    <hr class="line mt-5"/>

    <p class="text-left display-4 section-title">Highlights</p>
    <div class="row mt-5">
        <?php foreach ($highlights as $highlight):
            $image = $this->ArtifactImages->getMainImage($highlight['artifact_id']);
            ?>
            <div class='card home-card col-sm-12 col-md-6 col-lg-4 mb-5'>
                <img
                src = '<?=$image['thumbnail']?>'
                alt = 'Highlight <?= $highlight['title'].' '.$highlight['image_type']?> '
                class = 'card-img-top'>
                <div class='card-body'>
                    <h5 class='card-title'>
                        <?= $this->Html->link(strlen($highlight['title']) > 38 ? substr($highlight['title'], 0, 38)."..." : $highlight['title'], ['controller' => 'postings', 'action' => 'view', $highlight['id']]);?>
                    </h5>
                    <p class='card-text'>
                        <?=
                        strlen(strip_tags($highlight['body'])) > 100 ? substr(strip_tags($highlight['body']), 0, 100)."..." : strip_tags($highlight['body'])
                        // use above to trim extra card-length
                        ?>
                    </p>
                </div>
            </div>
        <?php endforeach;?>
    </div>

    <p class="text-left display-4 section-title ">News</p>
    <div class="row mt-5">
        <?php foreach ($newsarticles as $news):
            $image = $this->ArtifactImages->getMainImage($news['artifact_id']);
            ?>
            <div class='card home-card col-sm-12 col-md-6 col-lg-4 mb-5'>
                <img
                src = '<?=$image['thumbnail']?>'
                alt = 'News <?= $news['title'].' '.$news['image_type']?> '
                class = 'card-img-top'>
                <div class='card-body'>
                    <h5 class='card-title'>
                        <?= $this->Html->link(strlen($news['title']) > 38 ? substr($news['title'], 0, 38)."..." : $news['title'], ['controller' => 'postings', 'action' => 'view', $news['id']]);?>
                    </h5>
                    <p class="date-style"><?=date_format($news['publish_start'], 'Y-m-d')?></p>
                    <p class='card-text'>

                        <?=
                        strlen(strip_tags($news['body'])) > 100 ? substr(strip_tags($news['body']), 0, 100)."..." : strip_tags($news['body'])
                        // use above to trim extra card-length
                        ?>
                    </p>
                </div>
            </div>
        <?php endforeach;?>
    </div>
</div>
<?= $this->Scroll->toTop()?>
