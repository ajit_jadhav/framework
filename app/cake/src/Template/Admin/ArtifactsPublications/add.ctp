<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsPublication $artifactsPublication
 */
?>

<?php if ($flag == ''): ?>
    <div class="row justify-content-md-center">

        <div class="col-lg-7 boxed">
            <?= $this->Form->create($artifactsPublication) ?>
                <legend class="capital-heading"><?= __('Link Artifact and Publication') ?></legend>
                <table cellpadding="10" cellspacing="10">
                    <tr>
                        <td> Artifact ID: <td>
                        <td><?= $this->Form->control('artifact_id', ['label' => '', 'type' => 'text']); ?></td>
                    </tr>
                    <tr>
                        <td> Publication ID: <td>
                        <td><?=  $this->Form->control('publication_id', ['label' => '', 'type' => 'number']); ?></td>
                    </tr>
                    <tr>
                        <td> Exact Reference: <td>
                        <td><?= $this->Form->control('exact_reference', ['label' => '', 'type' => 'text', 'maxLength' => 20, 'required' => false]); ?></td>
                    </tr>
                    <tr>
                        <td> Publication Type: <td>
                        <td><?php   $options = [
                                        'primary' => 'primary',
                                        'electronic' => 'electronic',
                                        'citation' => 'citation',
                                        'collation' => 'collation',
                                        'history' => 'history',
                                        'other' => 'other'
                                    ];
                                    echo $this->Form->control('publication_type', ['label' => '', 'type' => 'select', 'options' => $options] );?></td>
                    </tr>
                    <tr>
                        <td> Publication Comments: <td>
                        <td><?= $this->Form->control('publication_comments', ['label' => '', 'type' => 'text']); ?></td>
                    </tr>
                </table>
                <?= $this->Form->submit('Submit',['class' => 'btn btn-primary']) ?>
            <?= $this->Form->end() ?>

        </div>

        <div class="col-lg boxed">
            <div class="capital-heading"><?= __('Related Actions') ?></div>
            <?= $this->Html->link(__('List All Links'), ['action' => 'index'], ['class' => 'btn-action']) ?>
            <br/>
            <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
            <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
            <br/>
            <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
            <?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add'], ['class' => 'btn-action']) ?>
            <br/>
        </div>

    </div>


<?php elseif ($flag == 'artifact'): ?>
    <div class="row justify-content-md-center">
        <div class="col-lg boxed">
        <legend class="capital-heading"><?= __('Publication Reference:') ?></legend>
        To be added
        </div>
    </div>    
    <div class="row justify-content-md-center">
        <div class="col-lg-7 boxed">
            <?= $this->Form->create($artifactsPublication, ['action' => 'add/'.'artifact'.'/'.$id]) ?>
                <legend class="capital-heading"><?= __('Link Artifact') ?></legend>
                <table cellpadding="10" cellspacing="10">
                    <tr>
                        <td> Publication ID: <td>
                        <td><?php echo $this->Form->control('publication_id', ['label' => '', 'type' => 'number', 'disabled' => 'disabled', 'value' => $id]);
                                    echo $this->Form->control('publication_id', ['type' => 'hidden', 'value' => $id]); ?></td>
                    </tr>
                    <tr>
                        <td> Artifact ID: <td>
                        <td><?= $this->Form->control('artifact_id', ['label' => '', 'type' => 'text']); ?></td>
                    </tr>
                    <tr>
                        <td> Exact Reference: <td>
                        <td><?= $this->Form->control('exact_reference', ['label' => '', 'type' => 'text', 'maxLength' => 20, 'required' => false]); ?></td>
                    </tr>
                    <tr>
                        <td> Publication Type: <td>
                        <td><?php   $options = [
                                        'primary' => 'primary',
                                        'electronic' => 'electronic',
                                        'citation' => 'citation',
                                        'collation' => 'collation',
                                        'history' => 'history',
                                        'other' => 'other'
                                    ];
                                    echo $this->Form->control('publication_type', ['label' => '', 'type' => 'select', 'options' => $options] );?></td>
                    </tr>
                    <tr>
                        <td> Publication Comments: <td>
                        <td><?= $this->Form->control('publication_comments', ['label' => '', 'type' => 'text']); ?></td>
                    </tr>
                </table>
                <?= $this->Form->submit('Submit',['class' => 'btn btn-primary']) ?>
            <?= $this->Form->end() ?>

        </div>

        <div class="col-lg boxed">
            <div class="capital-heading"><?= __('Related Actions') ?></div>
            <?= $this->Html->link(__('List All Links'), ['action' => 'index'], ['class' => 'btn-action']) ?>
            <br/>
            <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
            <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
            <br/>
            <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
            <?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add'], ['class' => 'btn-action']) ?>
            <br/>
        </div>

    </div>


    <h3 class="display-4 pt-3"><?= __('Linked Artifacts') ?></h3>

    <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
        <thead>
            <tr>
                <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
                <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('artifact_designation') ?></th>
                <th scope="col"><?= $this->Paginator->sort('publication_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('publication_designation') ?></th>
                <th scope="col"><?= $this->Paginator->sort('exact_reference') ?></th>
                <th scope="col"><?= $this->Paginator->sort('publication_type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('publication_comments') ?></th>
                <th scope="col"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($artifactsPublications as $artifactsPublication): ?>
            <tr>
                <!-- <td><?= $this->Number->format($artifactsPublication->id) ?></td> -->
                <td><?= $artifactsPublication->has('artifact') ? $this->Html->link($artifactsPublication->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $artifactsPublication->artifact->id]) : '' ?></td>
                <td><?= h($artifactsPublication->artifact->designation) ?></td>
                <td><?= $this->Html->link($artifactsPublication->publication_id, ['controller' => 'Publications', 'action' => 'view', $artifactsPublication->publication_id]) ?></td>
                <td><?= h($artifactsPublication->publication->designation) ?></td>
                <td><?= h($artifactsPublication->exact_reference) ?></td>
                <td><?= h($artifactsPublication->publication_type) ?></td>
                <td><?= h($artifactsPublication->publication_comments) ?></td>
                <td>
                    <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                            ['action' => 'edit', $artifactsPublication->id, 'artifact', $id],
                            ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                    <?= $this->Form->postLink(
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                            ['action' => 'delete', $artifactsPublication->id, 'artifact', $id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsPublication->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <div>
        <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
            <?= $this->Paginator->first() ?>
            <?= $this->Paginator->prev() ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next() ?>
            <?= $this->Paginator->last() ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>

<?php elseif ($flag == 'publication'): ?>
    <div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifactsPublication, ['action' => 'add'.'/'.$flag.'/'.$id]) ?>
            <legend class="capital-heading"><?= __('Link Publication') ?></legend>
            <table cellpadding="10" cellspacing="10">
                    <tr>
                        <td> Artifact ID: <td>
                        <td><?php echo $this->Form->control('artifact_id', ['label' => '', 'type' => 'number', 'disabled' => 'disabled', 'value' => $id]);
                                    echo $this->Form->control('artifact_id', ['type' => 'hidden', 'value' => $id]); ?></td>
                    </tr>
                    <tr>
                        <td> Publication ID: <td>
                        <td><?= $this->Form->control('publication_id', ['label' => '', 'type' => 'number']); ?></td>
                    </tr>
                    <tr>
                        <td> Exact Reference: <td>
                        <td><?= $this->Form->control('exact_reference', ['label' => '', 'type' => 'text', 'maxLength' => 20, 'required' => false]); ?></td>
                    </tr>
                    <tr>
                        <td> Publication Type: <td>
                        <td><?php   $options = [
                                        'primary' => 'primary',
                                        'electronic' => 'electronic',
                                        'citation' => 'citation',
                                        'collation' => 'collation',
                                        'history' => 'history',
                                        'other' => 'other'
                                    ];
                                    echo $this->Form->control('publication_type', ['label' => '', 'type' => 'select', 'options' => $options] );?></td>
                    </tr>
                    <tr>
                        <td> Publication Comments: <td>
                        <td><?= $this->Form->control('publication_comments', ['label' => '', 'type' => 'text']); ?></td>
                    </tr>
            </table>
            <?= $this->Form->submit('Submit',['class' => 'btn btn-primary']) ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('List All Links'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

    </div>


    <h3 class="display-4 pt-3"><?= __('Linked Publications') ?></h3>

    <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('artifact_designation') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publication_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publication_designation') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publication_reference') ?></th>
            <th scope="col"><?= $this->Paginator->sort('exact_reference') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publication_type') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publication_comments') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactsPublications as $artifactsPublication): ?>
        <tr>
            <!-- <td><?= $this->Number->format($artifactsPublication->id) ?></td> -->
            <td><?= $artifactsPublication->has('artifact') ? $this->Html->link($artifactsPublication->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $artifactsPublication->artifact->id]) : '' ?></td>
            <td><?= h($artifactsPublication->artifact->designation) ?></td>
            <td><?= $this->Html->link($artifactsPublication->publication_id, ['controller' => 'Publications', 'action' => 'view', $artifactsPublication->publication_id]) ?></td>
            <td><?= h($artifactsPublication->publication->designation) ?></td>
            <td> To be added </td>
            <td><?= h($artifactsPublication->exact_reference) ?></td>
            <td><?= h($artifactsPublication->publication_type) ?></td>
            <td><?= h($artifactsPublication->publication_comments) ?></td>
            <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $artifactsPublication->id, $flag, $id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $artifactsPublication->id, $flag, $id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsPublication->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
    </table>

    <div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first() ?>
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
        <?= $this->Paginator->last() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>

<?php elseif ($flag == 'bulk'): ?>

<?= $this->cell('BulkUpload::confirmation', [
                        isset($error_list) ? $error_list:null, 
                        isset($header) ? $header:null, 
                        isset($entities) ? $entities:null]); ?>

    <div class="boxed mx-0">
    <div class="capital-heading"><?= __('Related Actions') ?></div>

        <?= $this->Html->link(__('New Link'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add'], ['class' => 'btn-action']) ?>

    </div>

<?= $this->cell('BulkUpload', [
                        'ArtifactsPublications', 
                        isset($error_list) ? $error_list:null, 
                        isset($header) ? $header:null, 
                        isset($entities) ? $entities:null]); ?>

<?php endif; ?>