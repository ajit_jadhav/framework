<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Abbreviation $abbreviation
 */
?>
<div class="row justify-content-md-center">
    <!--<aside class="col-lg-7 boxed">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Abbreviation'), ['action' => 'edit', $abbreviation->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Abbreviation'), ['action' => 'delete', $abbreviation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $abbreviation->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Abbreviations'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Abbreviation'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>-->
    <div class="col-lg-7 boxed">
        <div class="abbreviations view content">
            <div class="capital-heading"><?= __('View Abbreviation') ?></div>
            <table class="table-bootstrap">
                <tr>
                    <th><?= __('Abb') ?></th>
                    <td><?= h($abbreviation->abbreviation) ?></td>
                </tr>
                <tr>
                    <th><?= __('Meaning') ?></th>
                    <td><?= h($abbreviation->fullform) ?></td>
                </tr>
                <tr>
                    <th><?= __('id') ?></th>
                    <td><?= $this->Number->format($abbreviation->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Publication Id') ?></th>
                    <td><?= $abbreviation->has('publication') ? $this->Html->link($abbreviation->publication->id, ['controller' => 'Publications', 'action' => 'view', $abbreviation->publication->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Type') ?></th>
                    <td><?= h($abbreviation->type) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>

    <div class="boxed mx-0">
    <?php if (empty($abbreviation->publications)): ?>
        <div class="capital-heading"><?= __('No Related Publications') ?></div>
    <?php else: ?>
        <div class="capital-heading"><?= __('Related Publications') ?></div>
                <table cellpadding="0" cellspacing="0" class="table-bootstrap">
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Bibtexkey') ?></th>
                            <th><?= __('Year') ?></th>
                            <th><?= __('Entry Type Id') ?></th>
                            <th><?= __('Address') ?></th>
                            <th><?= __('Annote') ?></th>
                            <th><?= __('Book Title') ?></th>
                            <th><?= __('Chapter') ?></th>
                            <th><?= __('Crossref') ?></th>
                            <th><?= __('Edition') ?></th>
                            <th><?= __('Editor') ?></th>
                            <th><?= __('How Published') ?></th>
                            <th><?= __('Institution') ?></th>
                            <th><?= __('Journal Id') ?></th>
                            <th><?= __('Month') ?></th>
                            <th><?= __('Note') ?></th>
                            <th><?= __('Number') ?></th>
                            <th><?= __('Organization') ?></th>
                            <th><?= __('Pages') ?></th>
                            <th><?= __('Publisher') ?></th>
                            <th><?= __('School') ?></th>
                            <th><?= __('Title') ?></th>
                            <th><?= __('Volume') ?></th>
                            <th><?= __('Publication History') ?></th>
                            <th><?= __('Abbreviation Id') ?></th>
                            <th><?= __('Series') ?></th>
                            <th><?= __('Oclc') ?></th>
                            <th><?= __('Designation') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($abbreviation->publications as $publications) : ?>
                        <tr>
                            <td><?= h($publications->id) ?></td>
                            <td><?= h($publications->bibtexkey) ?></td>
                            <td><?= h($publications->year) ?></td>
                            <td><?= h($publications->entry_type_id) ?></td>
                            <td><?= h($publications->address) ?></td>
                            <td><?= h($publications->annote) ?></td>
                            <td><?= h($publications->book_title) ?></td>
                            <td><?= h($publications->chapter) ?></td>
                            <td><?= h($publications->crossref) ?></td>
                            <td><?= h($publications->edition) ?></td>
                            <td><?= h($publications->editor) ?></td>
                            <td><?= h($publications->how_published) ?></td>
                            <td><?= h($publications->institution) ?></td>
                            <td><?= h($publications->journal_id) ?></td>
                            <td><?= h($publications->month) ?></td>
                            <td><?= h($publications->note) ?></td>
                            <td><?= h($publications->number) ?></td>
                            <td><?= h($publications->organization) ?></td>
                            <td><?= h($publications->pages) ?></td>
                            <td><?= h($publications->publisher) ?></td>
                            <td><?= h($publications->school) ?></td>
                            <td><?= h($publications->title) ?></td>
                            <td><?= h($publications->volume) ?></td>
                            <td><?= h($publications->publication_history) ?></td>
                            <td><?= h($publications->abbreviation_id) ?></td>
                            <td><?= h($publications->series) ?></td>
                            <td><?= h($publications->oclc) ?></td>
                            <td><?= h($publications->designation) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Publications', 'action' => 'view', $publications->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Publications', 'action' => 'edit', $publications->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Publications', 'action' => 'delete', $publications->id], ['confirm' => __('Are you sure you want to delete # {0}?', $publications->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
