<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsDateReferenced[]|\Cake\Collection\CollectionInterface $artifactsDateReferenced
 */
?>

<div class="artifactsDateReferenced index large-9 medium-8 columns content">
    <h3><?= __('Artifacts Date Referenced') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ruler_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('month_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('month_no') ?></th>
                <th scope="col"><?= $this->Paginator->sort('year_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('day_no') ?></th>
                <!-- <th scope="col" class="actions"><?= __('Actions') ?></th> -->
            </tr>
        </thead>
        <tbody>
            <?php foreach ($artifactsDateReferenced as $artifactsDateReferenced): ?>
            <tr>
                <td><?= $artifactsDateReferenced->has('artifact') ? $this->Html->link($artifactsDateReferenced->artifact->designation, ['controller' => 'Artifacts', 'action' => 'view', $artifactsDateReferenced->artifact->id]) : '' ?></td>
                <td><?= $artifactsDateReferenced->has('ruler') ? $this->Html->link($artifactsDateReferenced->ruler->ruler, ['controller' => 'Rulers', 'action' => 'view', $artifactsDateReferenced->ruler->id]) : '' ?></td>
                <td><?= $artifactsDateReferenced->has('month') ? $this->Html->link($artifactsDateReferenced->month->month_no, ['controller' => 'Months', 'action' => 'view', $artifactsDateReferenced->month->id]) : '' ?></td>
                <td><?= h($artifactsDateReferenced->month_no) ?></td>
                <td><?= $this->Number->format($artifactsDateReferenced->year_id) ?></td>
                <td><?= h($artifactsDateReferenced->day_no) ?></td>
                <!-- <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $artifactsDateReferenced->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $artifactsDateReferenced->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $artifactsDateReferenced->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsDateReferenced->id)]) ?>
                </td> -->
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first() ?>
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
        <?= $this->Paginator->last() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>
</div>
