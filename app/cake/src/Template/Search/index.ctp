<?php
    $filterOptions = ['r' => 'Relevance', 'l' => 'Lorem', 'ipsum' => 'dolor'];
    $showFilterDiv = true;
    $filterEntriesType = ["lorem", "ipsum" ,"dolor","sit"];
    $Objectfilters = ['Material' => ['a','b','c'], 'Museum Collections' => ['t','u','j'], 'Object type'=>['s','q','r'],'Period'=>['d','e','f'], 'Provenience'=>['k','l','m']];
    $Textualfilters = ['Genre' => ['a','b','c'], 'Language' => ['t','u','j']];
    $Publicationfilters = ['Authors' => ['a','b','c'], 'Date of publication' => ['t','u','j']];
    $appliedFilters = ["randomfilter1","anotherrandomfilter2"];
    $LayoutType = 1;
    echo $this->Html->css('filter');
    echo $this->Html->script('searchResult.js', ['defer' => true]);
?>

<div class="">
    <h1 class="container px-0 search-heading header-txt display-3 text-left"><?= __('Search the CDLI collection')?></h1>
    <?= $this->Form->create("", ['type'=>'get']) ?>
    <!-- 			<?= $this->Form->control('Publications'); ?>
        <?= $this->Form->control('Collections'); ?>
        <?= $this->Form->control('Proveniences'); ?>
        <?= $this->Form->control('Periods'); ?>
        <?= $this->Form->control('Transliteraton'); ?>
        <?= $this->Form->control('CDLI'); ?> -->
    <!-- 	<?php foreach ($result as $row) {
    echo "<br /> " . $row;
}
    ?> -->

    <div>
        <div id="dynamic_field">
            <div>

                <noscript>
                    <style>
                        .default-search-block, 
                        .default-add-search-btn{
                            display:none;
                        }
                    </style>
                    <div id="dynamic_field">
                        <div>
                            <div class="rectangle-2 container p-3"> 
                                <div class="search-page-grid" id="2">
                                    <label hidden="" for="input1">Query</label>
                                    <input type="text" id="input1" name="Publications" placeholder="Search for publications, provenience, collection no." aria-label="" aria-describedby="">
                                    <label hidden="" for="1">Category</label>
                                    <select class="form-group mb-0" id="1">
                                        <option value="Publications1">Publications</option>
                                        <option value="Collections1">Collections</option>
                                        <option value="Proveniences1">Proveniences</option>
                                        <option value="Periods1">Periods</option>
                                        <option value="Transliteraton1">Transliteraton</option>
                                        <option value="CDLI1">CDLI no</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    <?php for ($i = 1; $i<=2; $i++):?>
                        <div id="row<?=$i?>">
                            <div class="container rectangle-23 p-3">
                                <div class="align-items-baseline">
                                    <select type="dropdown" name="and" id="<?=$i?>" class="mb-3 mt-0 cdli-btn-light btn-and mr-3 float-left">
                                        <option>AND</option>
                                        <option>OR</option>
                                    </select>
                                    <div class="search-page-grid w-100-sm">
                                        <input type="text" class="form-control" id="input<?=$i?>" placeholder="Search for publications, provenience, collection no.">
                                        <select class="form-group mb-0" id="<?=$i?>">
                                            <option value="Publications<?=$i?>">Publications</option>
                                            <option value="Collections<?=$i?>">Collections</option>
                                            <option value="Proveniences<?=$i?>">Proveniences</option>
                                            <option value="Periods<?=$i?>">Periods</option> 
                                            <option value="Transliteraton<?=$i?>">Transliteraton</option>
                                            <option value="CDLI<?=$i?>">CDLI no</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endfor; ?>
                </noscript>

                <div class="rectangle-2 container p-3 default-search-block"> 
                    <div class="search-page-grid" id="2">
                        <label hidden for="input1">Query</label>
                        <input 
                            type="text" 
                            id="input1" 
                            name="Publications"
                            placeholder="Search for publications, provenience, collection no." 
                            aria-label="Search"
                        />
                        <label hidden for="1">Category</label>
                        <select class="form-group mb-0" id="1">
                            <option value="Publications1">Publications</option>
                            <option value="Collections1">Collections</option>
                            <option value="Proveniences1">Proveniences</option>
                            <option value="Periods1">Periods</option>
                            <option value="Transliteraton1">Transliteraton</option>
                            <option value="CDLI1">CDLI no</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container d-flex cdli-btn-group px-0 search-add-field-group">
        <?= $this->Form->button('Search', ['type' => 'submit', 'class' => 'btn cdli-btn-blue']); ?>
            <button type="button" name="add" id="add" class="btn cdli-btn-light default-add-search-btn">
                <span class="fa fa-plus-circle plus-icon"></span>Add search field
            </button>
            <a class="d-none d-lg-block search-links mr-5" href="#">Search settings</a>
            <a class="d-none d-lg-block search-links mr-5" href="/AdvancedSearch">Advanced search</a>
        <?= $this->Form->end()?>
    </div>    
    <hr class="line" />

    <?php if (isset($result)):?>
        <!-- Filter trigger button and dropdown and pagination controls -->
            <div class="d-none d-md-flex align-items-center justify-content-between mt-5">
                <div>
                    <button 
                        class="show-card-btn <?php if ($showFilterDiv) {
        echo "d-none";
    } ?> 
                        btn btn-outline-dark rounded-0 px-4" 
                        data-toggle="modal" 
                        data-target="#filter-modal">
                        Filter categories
                    </button>
                </div>

                <div class="d-flex align-items-center">
                    <?php if ($LayoutType == 1): ?>
                        <?= $this->Form->create("", ['type'=>'get','id'=>'sort-by-dd'])?>
                            <label for="filterSelect">Sort by:</label>
                            <?= $this->Form->select('sort-order:', $filterOptions, ['id' => 'filterSelect', 'class' => 'bg-white pl-1 ml-1 py-2 sort-by-dd'])?>
                        <?= $this->Form->end() ?>
                    <?php endif;?>
                    
                    <div class="d-none d-md-flex align-items-center ">
                        <?php if ($LayoutType == 1): ?>
                            <button class="btn bg-dark text-white border-dark ml-4 view-btn" aria-label="Toggle view to card layout" title="Card layout">
                                <span class="fa fa-th-large fa-2x pt-2 pb-1 px-1" aria-hidden="true"></span>
                            </button>
                            <button class="btn bg-white border-dark view-btn" aria-label="Toggle view to table layout" title="Table layout">
                                <span class="fa fa-bars fa-2x pt-2 pb-1 px-1" aria-hidden="true"></span>
                            </button>
                        <?php elseif ($LayoutType == 2): ?>
                            <button class="btn bg-white border-dark ml-4 view-btn" aria-label="Toggle view to card layout" title="Card layout">
                                <span class="fa fa-th-large fa-2x pt-2 pb-1 px-1" aria-hidden="true"></span>
                            </button>
                            <button class="btn bg-dark text-white border-dark view-btn" aria-label="Toggle view to table layout" title="Table layout">
                                <span class="fa fa-bars fa-2x pt-2 pb-1 px-1" aria-hidden="true"></span>
                            </button>
                        <?php else:?>
                            <!-- <button class="btn bg-white border-dark ml-4 view-btn">
                                <span class="fa fa-th-large fa-2x pt-2 pb-1 px-1" aria-hidden="true"></span>
                            </button>
                            <button class="btn bg-white border-dark border-left-0 view-btn">
                                <span class="fa fa-bars fa-2x pt-2 pb-1 px-1" aria-hidden="true"></span>
                            </button>
                            <button class="d-none btn bg-dark text-white border-dark view-btn">
                                <span class="fa fa-pie-chart fa-2x pt-2 pb-1 px-1" aria-hidden="true"></span>
                            </button> -->
                        <?php endif;?>                            
                    </div>
                </div>
            </div>
            
            <div class="d-md-none mt-5">
                <?php if ($LayoutType !=3): ?>
                    <div>
                        <?php if ($LayoutType == 1): ?>
                            <?= $this->Form->create("", ['type'=>'get','class'=>'sort-by-search-sm text-left'])?>
                                <label for="filterSelect">Sort by:</label>
                                <?= $this->Form->select('sort-order:', $filterOptions, ['id' => 'filterSelect', 'class' => 'bg-white py-2 '])?>
                            <?= $this->Form->end() ?>                
                        <?php endif;?>

                        <div class="d-flex align-items-center justify-content-between mt-4">
                            <button 
                                class="show-card-btn btn btn-outline-dark rounded-0 px-4" 
                                data-toggle="modal" 
                                data-target="#filter-modal">
                                Filter
                            </button>

                            <span class="text-muted">Showing <?= count($result);?> entries</span>
                        </div>
                    </div>
                <?php endif;?>

                <div class="d-flex align-items-center justify-content-between mt-4">
                        <button class="btn">
                            <?= $this->Html->link('Download', array(
                                'action' => 'view', $param, 'ext' => 'pdf'), ['class' => 'text-dark'])
                            ?>
                        </button>

                        <nav>
                            <ul class="pagination pagination-dark my-1 d-flex justify-content-md-end justify-content-center" id='page-bar'>
                                <li class="page-item" id="page-first"><button class="page-link text-dark" aria-label="Previous button" title="Previous results page"><span class="fa fa-angle-double-left"></span></button></li>
                                <li class="page-item" id="page-1"><button class="page-link text-dark page-custom border">-</button></li>
                                <li class="page-item" id="page-2"><button class="page-link text-dark page-custom">-</button></li>
                                <li class="page-item" id="page-3"><button class="page-link text-dark page-custom border">-</button></li>
                                <li class="page-item" id="page-last"><button class="page-link text-dark text-dark" aria-label="Next button" title="Next results page"><span class="fa fa-angle-double-right"></span></button></li>
                            </ul>
                        </nav>
                </div>
            </div>
            
        <!-- End Filter trigger button and  dropdown and pagination controls -->

        
        <!-- Download dropdown and pagination controls -->
        <?php if ($LayoutType !=3): ?>
            <div class="d-none d-md-flex align-items-center justify-content-between mt-5">
                <div>
                    <span class="text-muted">Showing <?= count($result);?> entries</span>
                    <button class="ml-2 btn p-0">
                        <?= $this->Html->link('Download', array(
                            'action' => 'view', $param, 'ext' => 'pdf'), ['class' => 'text-dark btn'])
                        ?>
                    </button>
                </div>

                <nav>
                    <ul class="pagination pagination-dark my-1 d-flex justify-content-md-end justify-content-center" id='page-bar'>
                        <li class="page-item" id="page-first"><button class="page-link text-dark" aria-label="Previous button" title="Previous results page"><span class="fa fa-angle-double-left"></span></button></li>
                        <li class="page-item" id="page-1"><button class="page-link text-dark page-custom border">-</button></li>
                        <li class="page-item" id="page-2"><button class="page-link text-dark page-custom">-</button></li>
                        <li class="page-item" id="page-3"><button class="page-link text-dark page-custom border">-</button></li>
                        <li class="page-item" id="page-last"><button class="page-link text-dark" aria-label="Next button" title="Next results page"><span class="fa fa-angle-double-right"></span></button></li>
                    </ul>
                </nav>
            </div>
        <?php endif;?>
        <!-- End Download dropdown and pagination controls -->
        
        <?php if ($LayoutType == 2):?>
            <div class="mt-5 rectangle-2 p-3 extended-search-actions" id="sort-options-bar"> 
                <span class="font-weight-bold">SORT OPTIONS:</span>
        
                <?= $this->Form->create("", ['type'=>'get','class'=>'sort-options'])?>
                    <div>
                        <label for="sort-options-1">1.</label>
                        <?= $this->Form->select('sort-order:', ['option1'], ['id' => 'sort-options-1', 'class' => 'bg-white pl-1 ml-1 py-2 sort-by-dd'])?>
                    </div>

                    <div>
                        <label for="sort-options-2">2.</label>
                        <?= $this->Form->select('sort-order:', ['option2'], ['id' => 'sort-options-2', 'class' => 'bg-white pl-1 ml-1 py-2 sort-by-dd'])?>
                    </div>

                    <div>
                        <label for="sort-options-3">3.</label>
                        <?= $this->Form->select('sort-order:', ['option3'], ['id' => 'sort-options-3', 'class' => 'bg-white pl-1 ml-1 py-2 sort-by-dd'])?>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        <?php endif;?>    

        <?php if (count($appliedFilters) > 0 && $LayoutType!=3): ?>
            <div class="mt-4 rectangle-2 p-3 extended-search-actions"> 
                <span class="font-weight-bold">APPLIED FILTERS:</span>
                <div>
                    <?php foreach ($appliedFilters as $filter):?>
                        <div class="btn bg-white p-2 m-1">
                            <?=$filter?> 
                            <button class="btn bg-white p-0" aria-label="Remove filter button" title="Remove this filter from current results">
                                <span class="fa fa-times-circle" aria-hidden="true"></span>
                            </button>
                        </div>
                    <?php endforeach;?>
                </div>
                <a href="#" class="text-right cdli-blue">Clear all</a>
            </div>
        <?php endif; ?>
        

        <div class="row">
            <?php if ($showFilterDiv): ?>
                <div class="sideFilterBlock d-none d-md-block col-md-3 mt-5">
                    
                    <p class="text-left font-weight-bold sidebarFilter-p">FILTER OPTIONS</p>
                    <div class="d-flex mb-4">
                            <button class="btn bg-white cdli-blue p-0" id="filter-div-expand" onclick="expandAll()">Expand all</button>
                            <button class="btn bg-white cdli-blue text-dark p-0 ml-2" id="filter-div-collapse" onclick="collapseAll()" disabled>Collapse all</button>
                    </div>

                    <!--Filters  -->
                    <?= $this->Form->create("", ['type'=>'get','class'=>'accordion text-left']) ?>
                                <div class="filter-group--header">Show entries with</div>
                                <div class="border p-3">
                                    <div class="row">
                                        <?php foreach ($filterEntriesType as $entry):?>
                                            <div class="col-sm-12 filter-checkbox">
                                                <?=$this->Form->control($entry, ['type' => 'checkbox','value' => $entry]);?>
                                            </div>
                                        <?php endforeach;?>
                                    </div>
                                </div>   
                                
                                <div class="filter-group--header mt-3">Object</div>
                                
                                <?php foreach ($Objectfilters as $entry => $xfilters):?>
                                    <?= $this->Accordion->partOpen($entry, $entry) ?>
                                        <div class="border filter-modal-block p-3">
                                            <div class="row">
                                                <?php foreach ($xfilters as $filter):?>
                                                    <div class="col-sm-12">
                                                        <?=$this->Form->control($filter, ['type' => 'checkbox','value' => $entry]);?>
                                                    </div>
                                                <?php endforeach;?>
                                            </div>
                                        </div>
                                    <?= $this->Accordion->partClose() ?>
                                
                                <?php endforeach;?>

                                <div class="filter-group--header mt-3">Textual data</div>
                                
                                <?php foreach ($Textualfilters as $entry => $xfilters):?>
                                    <?= $this->Accordion->partOpen($entry, $entry) ?>                               
                                        <div class="border filter-modal-block p-3">
                                            <div class="row">
                                                <?php foreach ($xfilters as $filter):?>
                                                    <div class="col-sm-12">
                                                        <?=$this->Form->control($filter, ['type' => 'checkbox','value' => $entry]);?>
                                                    </div>
                                                <?php endforeach;?>
                                            </div>
                                        </div>
                                    <?= $this->Accordion->partClose() ?>
                                
                                <?php endforeach;?>

                                <div class="filter-group--header mt-3">Publication</div>
                                
                                <?php foreach ($Publicationfilters as $entry => $xfilters):?>
                                    <?= $this->Accordion->partOpen($entry, $entry) ?>
                                        <div class="border filter-modal-block p-3">
                                            <div class="row">
                                                <?php foreach ($xfilters as $filter):?>
                                                    <div class="col-sm-12 col-md-4 col-lg-3">
                                                        <?=$this->Form->control($filter, ['type' => 'checkbox','value' => $entry]);?>
                                                    </div>
                                                <?php endforeach;?>
                                            </div>
                                        </div>
                                    <?= $this->Accordion->partClose() ?>
                                
                                <?php endforeach;?>

                                <div class="d-flex justify-content-center mt-5">
                                    <?=$this->form->submit("Apply", ['id' => 'filter-button-apply' , 'class' => 'btn rounded-0 px-5 py-2'])?>
                                </div>
                                
                            <?= $this->Form->end()?>
                        <!-- end Filters -->
                </div>
            <?php endif; ?>
            <?php echo ($showFilterDiv == true) ?
            
            "<div class='col-sm-12 col-md-9'>":"<div class='overflow-wrapper w-100'>"; ?>
    
                <?php if ($LayoutType == 1): ?>
                    <!-- show card layout -->
                    <div class="search-grid-layout text-left container mt-5" id="card-view">
                            <?php foreach ($result as $row):?>
                                <div class="row mb-5">
                                    <div class="col-lg-3 col-md-4 bg-dark d-none d-md-block">
                                        <div class="search-media">
                                            <div>
                                                <a href="#">
                                                    <figure>
                                                        <img src="https://cdli.ucla.edu/dl/tn_photo/P000001.jpg" alt="Artifact photograph">
                                                        <figcaption>View full image <span class="fa fa-share-square-o" aria-hidden="true"></span></figcaption>
                                                    </figure>
                                                </a>
                                            </div>
                                        <div>
                                        
                                        <p class="text-center">
                                            <a href="#" class="text-white"><u>View line art</u></a>
                                        </p>

                                        <div class="mx-3 mt-3 text-white d-md-none d-lg-flex d-xl-flex justify-content-between">
                                            <p>View RTI:</p>
                                            <a href="#" class="text-white"><u>Observe</u></a>
                                            <a href="#" class="text-white"><u>Reverse</u></a>
                                        </div>
                                        
                                        <div class="mx-3 mt-3 text-white d-md-flex d-lg-none d-xl-none justify-content-between">
                                            <p>View RTI:</p>
                                            
                                            <span class="w-50">
                                                <a href="#" class="text-white"><u>Observe</u></a>
                                                <a href="#" class="text-white"><u>Reverse</u></a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                    <div class="search-result-card border px-4 py-5 col-lg-9 col-md-8">
                                        <?php if (strlen(($row["_matchingData"]["Publications"]["designation"]))):?>
                                            <?= $this->Html->link(
                $row["_matchingData"]["Publications"]["designation"],
                'showmore?CDLI='.$row["id"].'&Publication='.$row["_matchingData"]["Publications"]["id"],
                ['class' => 'title active link', 'target' => '_blank']
            );?>
                                        <?php endif;?>
                                        <?php echo "<p class='mt-3'><b>Author:</b> ".$row["_matchingData"]["Authors"]["author"]."</p>";?>
                                        <?php echo "<p><b>Publications Date:</b> ".$row["_matchingData"]["Publications"]["year"]."</p>"; ?>
                                        <?php echo "<p><b>CDLI No.:</b> ".$row["id"]."</p>";?>
                                        <?php echo "<p><b>Collection:</b> ".$row["_matchingData"]["Collections"]["collection"]."</p>";?>
                                        <?php echo "<p><b>Provenience:</b> ".$row["_matchingData"]["Proveniences"]["provenience"]."</p>";?>
                                        <?php echo "<p><b>Period:</b> ".$row["_matchingData"]["Periods"]["period"]."</p>";?>
                                        <?php echo "<p><b>Object Type:</b> ".$row["_matchingData"]["ArtifactTypes"]["artifact_type"]."</p>";?>
                                        <?php echo "<p><b>Material:</b> ".$row["_matchingData"]["Materials"]["material"]."</p>";?>
                                        <?php echo "<p class='mt-3'><b>Transliteration/Translation:</b><br/><br/>".$row["_matchingData"]["Inscriptions"]["transliteration"]."</p>";?>
                                        <button class="btn mt-3">
                                            <?php
                                                echo $this->Html->link(
                                            "Show more",
                                            'showmore?CDLI='.$row["id"].'&Publication='.$row["_matchingData"]["Publications"]["id"],
                                            ['class' => ' text-dark', 'target' => '_blank']
                                        );
                                            ?>
                                        </button>
                                    </div>
                                </div>
                            <?php endforeach;?>
                        </div>
                    <!-- end show card layout -->
                <?php elseif ($LayoutType == 2):?>
                    <!-- show table layout -->
                    <div class="overflow-wrapper mt-5" id="table-view">
                            <div class="search-table-layout text-left d-flex border-bottom">
                                <table class="fixed-table">
                                <tr>
                                            <th class="border-right">Primary Publication</th>
                                </tr>
                                    <?php foreach ($result as $row):?>
                                        <?php
                                            $publicationText = strlen($row["_matchingData"]["Publications"]["designation"]) > 60
                                            ? substr($row["_matchingData"]["Publications"]["designation"], 0, 60). "..." : $row["_matchingData"]["Publications"]["designation"];
                                        ?>
                                        <tr class="border-top">    
                                            <td class="border-right">
                                                <?php if (strlen($publicationText)):?>
                                                    <?=
                                                        $this->Html->link(
                                                            $publicationText,
                                                            'showmore?CDLI='.$row["id"].'&Publication='.$row["_matchingData"]["Publications"]["id"],
                                                            ['class' => 'active', 'target' => '_blank']
                                                        )
                                                    ?>
                                                <?php else:?>
                                                    <?= "NA" ?>
                                                <?php endif;?>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                </table>
                                <table class="scrollable-table">
                                    <tr>
                                            <th>CDLI NO.</th>
                                            <th>Period</th>
                                            <th>Publication Date</th>
                                            <th>Provenience</th>
                                            <th>Author</th>
                                            <th>Object Type</th>
                                            <th>Collection</th>
                                            <th>Material</th>
                                            <th>Transliteration/Translation</th>
                                    </tr>
                                    <?php foreach ($result as $row):?>
                                        <tr class="border-top">
                                            <td>
                                                <?php echo $row["id"];?>
                                            </td>
                                            
                                            <td>
                                                <?php echo $row["_matchingData"]["Periods"]["period"];?>
                                            </td>
                                            <td>
                                                <?php echo $row["_matchingData"]["Publications"]["year"]; ?>
                                            </td>
                                            <td>
                                                <?php echo $row["_matchingData"]["Proveniences"]["provenience"];?>
                                            </td>
                                            
                                            <td>
                                                <?php echo $row["_matchingData"]["Authors"]["author"];?>
                                            </td>
                                            
                                            <td>
                                                <?php echo $row["_matchingData"]["ArtifactTypes"]["artifact_type"];?>
                                            </td>
                                            
                                            <td>
                                                <?php echo strlen($row["_matchingData"]["Collections"]["collection"]) > 35 ? substr($row["_matchingData"]["Collections"]["collection"], 0, 35)."..." :  $row["_matchingData"]["Collections"]["collection"] ?>
                                            </td>
                                            
                                            <td>
                                                <?php echo $row["_matchingData"]["Materials"]["material"];?>
                                            </td>
                                            <td>
                                                <?php echo strlen($row["_matchingData"]["Inscriptions"]["transliteration"]) > 250 ? substr($row["_matchingData"]["Inscriptions"]["transliteration"], 0, 250)."..." : $row["_matchingData"]["Inscriptions"]["transliteration"]?>
                                            </td>
                                        
                                        </tr>
                                    <?php endforeach;?>
                                </table>
                            </div>
                        </div> 
                    <!-- end show table layout -->

                <?php else:?>
                    <div class="my-5">
                        <p class="my-5">
                            Stats will come here
                        </p>
                    </div>
                <?php endif;?>
                </div>
            
            </div>
        </div>
        
        <!-- pagination bottom -->
        <?php if ($LayoutType !=3):?>
            <div class="mt-5 d-md-flex flex-row-reverse justify-content-between align-items-baseline search-bottom-controls">
                <nav class="mb-4">
                    <ul class="pagination pagination-dark my-1 d-flex justify-content-md-end justify-content-center" id='page-bar'>
                        <li class="page-item" id="page-first"><button class="page-link text-dark" aria-label="Previous button" title="Previous results page"><span class="fa fa-angle-double-left"></span></button></li>
                        <li class="page-item" id="page-1"><button class="page-link text-dark page-custom border">-</button></li>
                        <li class="page-item" id="page-2"><button class="page-link text-dark page-custom">-</button></li>
                        <li class="page-item" id="page-3"><button class="page-link text-dark page-custom border">-</button></li>
                        <li class="page-item" id="page-last"><button class="page-link text-dark" aria-label="Next button" title="Next results page"><span class="fa fa-angle-double-right"></span></button></li>
                    </ul>
                </nav>
                <div class="d-flex justify-content-between align-items-baseline">
                    <p class="text-muted mr-2">Results per page: </p>
                    <span>
                        <a href="#" class="mx-2">10</a>
                        <a href="#" class="mx-2">100</a>
                        <a href="#" class="mx-2">All</a>
                    </span>
                </div>
            </div>
        
        <?=$this->Scroll->toTop();?>
        
        <?php endif;?>
        <!-- end pagination bottom -->
        
        <!-- filter modal -->
            <?php if (!$showFilterDiv):?>
            <div class="modal fade bd-example-modal-lg" id="filter-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered" id="search-modal-dialog">
                    <div class="modal-content filter-modal" id="search-modal-content">
                        <div class="d-flex flex-row justify-content-between">
                            <h5 class="filter-heading">Filter options</h5>
                            <button type="button" class="filter-close" data-dismiss="modal">
                                <span class="fa fa-times my-auto" aria-hidden="true"></span>
                            </button>
                        </div>
                        
                        <div class="d-flex justify-content-end mt-2">
                            <button class="btn bg-white text-primary" id="side-div-expand" onclick="expandAll()">Expand All</button>
                            <button class="btn bg-white text-primary text-dark ml-2" id="side-div-collapse" onclick="collapseAll()" disabled>Collapse all</button>
                        </div>
            
                        <!--Filters  -->
                            <?= $this->Form->create("", ['type'=>'get','class'=>'mt-2 accordion text-left']) ?>
                                <div class="filter-group--header mt-3">Show entries with</div>
                                <div class="border p-3">
                                    <div class="row">
                                        <?php foreach ($filterEntriesType as $entry):?>
                                            <div class="col-sm-12 col-md-4 col-lg-3 filter-checkbox">
                                                <?=$this->Form->control($entry, ['type' => 'checkbox','value' => $entry]);?>
                                            </div>
                                        <?php endforeach;?>
                                    </div>
                                </div>   
                                
                                <div class="filter-group--header mt-3">Object</div>
                                
                                <?php foreach ($Objectfilters as $entry => $xfilters):?>
                                    <?= $this->Accordion->partOpen($entry, $entry) ?>
                                        <div class="border filter-modal-block p-3">
                                            <div class="row">
                                                <?php foreach ($xfilters as $filter):?>
                                                    <div class="col-sm-12 col-md-4 col-lg-3">
                                                        <?=$this->Form->control($filter, ['type' => 'checkbox','value' => $entry]);?>
                                                    </div>
                                                <?php endforeach;?>
                                            </div>
                                        </div>
                                    <?= $this->Accordion->partClose() ?>
                                
                                <?php endforeach;?>

                                <div class="filter-group--header mt-3">Textual data</div>
                                
                                <?php foreach ($Textualfilters as $entry => $xfilters):?>
                                    <?= $this->Accordion->partOpen($entry, $entry) ?>
                                        <div class="border filter-modal-block p-3">
                                            <div class="row">
                                                <?php foreach ($xfilters as $filter):?>
                                                    <div class="col-sm-12 col-md-4 col-lg-3">
                                                        <?=$this->Form->control($filter, ['type' => 'checkbox','value' => $entry]);?>
                                                    </div>
                                                <?php endforeach;?>
                                            </div>
                                        </div>
                                    <?= $this->Accordion->partClose() ?>
                                
                                <?php endforeach;?>

                                <div class="filter-group--header mt-3">Publication</div>
                                
                                <?php foreach ($Publicationfilters as $entry => $xfilters):?>
                                    <?= $this->Accordion->partOpen($entry, $entry) ?>
                                        <div class="border filter-modal-block p-3">
                                            <div class="row">
                                                <?php foreach ($xfilters as $filter):?>
                                                    <div class="col-sm-12 col-md-4 col-lg-3">
                                                        <?=$this->Form->control($filter, ['type' => 'checkbox','value' => $entry]);?>
                                                    </div>
                                                <?php endforeach;?>
                                            </div>
                                        </div>
                                    <?= $this->Accordion->partClose() ?>
                                
                                <?php endforeach;?>

                                <div class="d-flex justify-content-center mt-4">
                                    <?=$this->form->submit("Apply", ['id' => 'filter-button-apply' , 'class' => 'btn cdli-btn-blue rounded-0 px-5 py-2'])?>
                                </div>
                                
                            
                            <?= $this->Form->end()?>
                        <!-- end Filters -->
                    </div>
                </div>
            </div>
        <!-- end filter modal -->
        <?php endif;?>
    <?php endif;?>
</div>