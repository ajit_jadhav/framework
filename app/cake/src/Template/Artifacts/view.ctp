<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artifact $artifact
 */

$CDLI_NO = "P" . str_pad($artifact->id, 6, '0', STR_PAD_LEFT)
?>

<?= $this->Html->link('export-csv', [
    'controller' => 'Artifacts',
    'action' => 'view',
    $artifact->id,
    '_ext' => 'csv'
]) ?>

<?= $this->Html->link('export-xlsx', [
    'controller' => 'Artifacts',
    'action' => 'view',
    $artifact->id,
    '_ext' => 'xlsx'
]) ?>

<?= $this->Html->link('export-tsv', [
    'controller' => 'Artifacts',
    'action' => 'view',
    $artifact->id,
    '_ext' => 'tsv'
]) ?>

<?= $this->Html->link('export-json', [
    'controller' => 'Artifacts',
    'action' => 'view',
    $artifact->id,
    '_ext' => 'json'
]) ?>

<?= $this->Html->link('export-xml', [
    'controller' => 'Artifacts',
    'action' => 'view',
    $artifact->id,
    '_ext' => 'xml'
]) ?>

<main id="artifact">
    <a id="back" href="#" onclick="history.back()"><i class="fa fa-chevron-left"></i> Back to Search Results</a>
    <h1><?= h($artifact->designation) ?> (<?= h($CDLI_NO) ?>)</h1>
    <h2>
        <?php if (!empty($artifact->genres)): ?>
            <?= h($artifact->genres[0]->genre) ?>,
        <?php endif; ?>
        <?php if (!empty($artifact->artifact_type)): ?>
            <?= h($artifact->artifact_type->artifact_type) ?>,
        <?php endif; ?>
        <?php if (!empty($artifact->provenience)): ?>
            <?= h($artifact->provenience->provenience) ?>
        <?php endif; ?>
        <?php if (!empty($artifact->period)): ?>
            in <?= h($artifact->period->period) ?>
        <?php endif; ?>
        <?php if (!empty($artifact->collections)): ?>
            and kept at <?= h($artifact->collections[0]->collection) ?>
        <?php endif; ?>
    </h2>

    <div id="artifact-media">
        <div>
            <a href="https://cdli.ucla.edu/dl/photo/<?= $CDLI_NO ?>.jpg">
                <figure>
                    <img src="https://cdli.ucla.edu/dl/tn_photo/<?= $CDLI_NO ?>.jpg" />
                    <figcaption>View full image <i class="fa fa-external-link"></i></figcaption>
                </figure>
            </a>
        </div>
        <footer>
          <a href="https://cdli.ucla.edu/dl/line_art/<?= $CDLI_NO ?>.jpg">View line art</a>
          &bullet;
          <a href="https://cdli.ucla.edu/dl/line_art/<?= $CDLI_NO ?>_ld.jpg">View detailed line art</a>
          &bullet;
          View RTI
          &bullet;
          pdf
        </footer>
    </div>

    <div id="artifact-summary">
        <h2>Summary</h2>

        <div>
            <div>
              <h3>Musuem Collections</h3>
              <?php foreach ($artifact->collections as $collection): ?>
                <?= $this->Html->link($collection->collection, ['controller' => 'Collections', 'action' => 'view', $collection->id]) ?>
              <?php endforeach; ?>
            </div>

            <div>
              <h3>Material</h3>
              <?php foreach ($artifact->materials as $material): ?>
                <?= $this->Html->link($material->material, ['controller' => 'Materials', 'action' => 'view', $material->id]) ?>
              <?php endforeach; ?>
            </div>

            <div>
              <h3>Period</h3>
              <?php if (!empty($artifact->period)): ?>
                <?= $this->Html->link($artifact->period->period, ['controller' => 'Periods', 'action' => 'view', $artifact->period->id]) ?>
              <?php endif; ?>
            </div>

            <div>
              <h3>Genre / Subgenre</h3>
              <?php foreach ($artifact->genres as $genre): ?>
                <?= $this->Html->link($genre->genre, ['controller' => 'Genres', 'action' => 'view', $genre->id]) ?>
              <?php endforeach; ?>
            </div>

            <div>
              <h3>Provenience</h3>
              <?php if (!empty($artifact->provenience)): ?>
                <?= $this->Html->link($artifact->provenience->provenience, ['controller' => 'Proveniences', 'action' => 'view', $artifact->provenience->id]) ?>
              <?php endif; ?>
            </div>

            <div>
              <h3>Language</h3>
              <?php foreach ($artifact->languages as $language): ?>
                <?= $this->Html->link($language->language, ['controller' => 'Languages', 'action' => 'view', $language->id]) ?>
              <?php endforeach; ?>
            </div>

            <div>
              <h3>Artifact Type</h3>
              <?php if (!empty($artifact->artifact_type)): ?>
                <?= $this->Html->link($artifact->artifact_type->artifact_type, ['controller' => 'ArtifactTypes', 'action' => 'view', $artifact->artifact_type->id]) ?>
              <?php endif; ?>
            </div>
        </div>
    </div>

    <div id="artifact-detail" class="accordion">
        <?= $this->Accordion->partOpen('inscriptions', 'Inscriptions') ?>
            <?php foreach ($artifact->inscriptions as $inscription): ?>
            <div>
              <p>No. <?= h($inscription->id) ?>:</p>
              <pre><?= h($inscription->transliteration_clean) ?></pre>
            </div>
            <?php endforeach; ?>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('collections', 'Collections') ?>
            <ul>
                <?php foreach ($artifact->collections as $collection): ?>
                <li>
                    <?= $this->Html->link($collection->collection, ['controller' => 'Collections', 'action' => 'view', $collection->id]) ?>
                </li>
                <?php endforeach; ?>
            </ul>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('physical_information', 'Physical Information') ?>
            <ul>
                <li>
                    <?= __('Object type') ?>:
                    <?php foreach ($artifact->genres as $genre): ?>
                        <?= $this->Html->link($genre->genre, ['controller' => 'Genres', 'action' => 'view', $genre->id]) ?>
                    <?php endforeach; ?>
                </li>
                <li>
                    <?= __('Is Object Type Uncertain') ?>:
                    <?= $artifact->is_object_type_uncertain ? __('Yes') : __('No'); ?>
                </li>
                <li>
                    <?= __('Material') ?>:
                    <?php foreach ($artifact->materials as $material): ?>
                        <?= $this->Html->link($material->material, ['controller' => 'Materials', 'action' => 'view', $material->id]) ?>
                    <?php endforeach; ?>
                </li>
                <li>
                    <?= __('Measurements (mm)') ?>:
                    <?= h($artifact->height) ?> x
                    <?= h($artifact->width) ?> x
                    <?= h($artifact->thickness) ?>
                </li>
                <li>
                    <?= __('Weight') ?>:
                    <?= h($artifact->weight) ?>
                </li>
                <li>
                    <?= __('Artifact Preservation') ?>:
                    <?= h($artifact->artifact_preservation) ?>
                </li>
                <li>
                    <?= __('Condition Description') ?>:
                    <?= h($artifact->condition_description) ?>
                </li>
                <li>
                    <?= __('Join Information') ?>:
                    <?= h($artifact->join_information) ?>
                </li>
                <li>
                    <?= __('Seal no.') ?>:
                    <?= h($artifact->seal_no) ?>
                </li>
                <li>
                    <?= __('Seal information') ?>:
                    <?= $this->Text->autoParagraph(h($artifact->seal_information)) ?>
                </li>
            </ul>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('publications', 'Publication Data') ?>
            <ul>
                <?php foreach ($artifact->publications as $publication): ?>
                <li>
                    <?= $this->Html->link($publication->_joinData->publication_type, ['controller' => 'Publications', 'action' => 'view', $publication->id]) ?>
                    <?php if (!empty($publication->designation)): ?>
                        : <?= h($publication->designation) ?>
                    <?php endif; ?>
                    <?php foreach ($publication->authors as $author): ?>
                        (<?= $this->Html->link($author->author, ['controller' => 'Authors', 'action' => 'view', $author->id]) ?>)
                    <?php endforeach; ?>
                </li>
                <?php endforeach; ?>
            </ul>

            <h3>Author comments</h3>
            <?= $this->Text->autoParagraph(h($artifact->primary_publication_comments)) ?>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('identifiers', 'Identifiers') ?>
            <ul>
              <li>
                  <?= __('Ark No.') ?>:
                  <?= h($artifact->ark_no) ?>
              </li>
              <li>
                  <?= __('Composite No.') ?>:
                  <?= h($artifact->composite_no) ?>
              </li>
              <li>
                  <?= __('Museum No.') ?>:
                  <?= h($artifact->museum_no) ?>
              </li>
              <li>
                  <?= __('Accession No.') ?>:
                  <?= h($artifact->accession_no) ?>
              </li>
              <li>
                  <?= __('Designation') ?>:
                  <?= h($artifact->designation) ?>
              </li>
              <li>
                  <?= __('Custom Designation') ?>:
                  <?= h($artifact->custom_designation) ?>
              </li>
            </ul>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('provenience', 'Provenience') ?>
            <ul>
                <li>
                    <?= __('Provenience') ?>:
                    <?php if (!empty($artifact->provenience)): ?>
                      <?= $this->Html->link($artifact->provenience->provenience, ['controller' => 'Proveniences', 'action' => 'view', $artifact->provenience->id]) ?>
                    <?php endif; ?>
                </li>
                <li>
                    <?= __('Is Provenience Uncertain') ?>:
                    <?= $artifact->is_provenience_uncertain ? __('Yes') : __('No'); ?>
                </li>
                <li>
                    <?= __('Provenience Comments') ?>:
                    <?= $this->Text->autoParagraph(h($artifact->provenience_comments)); ?>
                </li>
                <li>
                    <?= __('Elevation') ?>:
                    <?= h($artifact->elevation) ?>
                </li>
                <li>
                    <?= __('Stratigraphic Level') ?>:
                    <?= h($artifact->stratigraphic_level) ?>
                </li>
                <li>
                    <?= __('Excavation No') ?>:
                    <?= h($artifact->excavation_no) ?>
                </li>
                <li>
                    <?= __('Findspot Square') ?>:
                    <?= h($artifact->findspot_square) ?>
                </li>
                <li>
                    <?= __('Findspot Comments') ?>:
                    <?= $this->Text->autoParagraph(h($artifact->findspot_comments)) ?>
                </li>
            </ul>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('chronology', 'Chronology') ?>
            <ul>
                <li>
                    <?= __('Period') ?>:
                    <?php if (!empty($artifact->period)): ?>
                      <?= $this->Html->link($artifact->period->period, ['controller' => 'Periods', 'action' => 'view', $artifact->period->id]) ?>
                    <?php endif; ?>
                </li>
                <li>
                    <?= __('Is Period Uncertain') ?>:
                    <?= $artifact->is_period_uncertain ? __('Yes') : __('No'); ?>
                </li>
                <li>
                    <?= __('Period Comments') ?>:
                    <?= $this->Text->autoParagraph(h($artifact->period_comments)) ?>
                </li>
                <li>
                    <?= __('Accounting Period') ?>:
                    <?= $this->Number->format($artifact->accounting_period) ?>
                </li>
                <li>
                    <?= __('Dates Referenced') ?>:
                    <?= h($artifact->dates_referenced) ?>
                </li>
                <li>
                    <?= __('Date Comments') ?>:
                    <?= h($artifact->date_comments) ?>
                </li>
                <li>
                  <?= __('Alternative Years') ?>:
                  <?= h($artifact->alternative_years) ?>
                </li>
            </ul>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('credits', 'Credits') ?>
            <ul>
              <?php foreach ($artifact->credits as $credit): ?>
              <li>
                  <?= $this->Html->link($credit->author->author, ['controller' => 'Authors', 'action' => 'view', $credit->author->id]) ?>
                  (<?= h($credit->date) ?>)<!--
                  --><?php if (!empty($credit->comments)): ?>: <?= h($credit->comments) ?><?php endif; ?>
              </li>
              <?php endforeach; ?>
            </ul>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('administrative', 'Administrative') ?>
            <ul>
                <li>
                    <?= __('CDLI Collation') ?>:
                    <?= h($artifact->cdli_collation) ?>
                </li>
                <li>
                    <?= __('Created') ?>:
                    <?= h($artifact->created) ?>
                </li>
                <li>
                    <?= __('Created By') ?>:
                    <?= h($artifact->created_by) ?>
                </li>
                <li>
                    <?= __('Modified') ?>:
                    <?= h($artifact->modified) ?>
                </li>
                <li>
                    <?= __('Is Public') ?>:
                    <?= $artifact->is_public ? __('Yes') : __('No'); ?>
                </li>
                <li>
                    <?= __('Is Atf Public') ?>:
                    <?= $artifact->is_atf_public ? __('Yes') : __('No'); ?>
                </li>
                <li>
                    <?= __('Are Images Public') ?>:
                    <?= $artifact->are_images_public ? __('Yes') : __('No'); ?>
                </li>
                <li>
                    <?= __('Is School Text') ?>:
                    <?= $artifact->is_school_text ? __('Yes') : __('No'); ?>
                </li>
            </ul>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('external_resources', 'External Resources') ?>\
            <ul>
                <?php foreach ($artifact->external_resources as $external_resource): ?>
                <li>
                    <?= $this->Html->link(
                      $external_resource->external_resource,
                      $external_resource->base_url . $external_resource->_joinData->external_resource_key
                    ) ?>
                </li>
                <?php endforeach; ?>
            </ul>
        <?= $this->Accordion->partClose() ?>
    </div>

    <div>
        <?= __('General Comments') ?>:
        <?= $this->Text->autoParagraph(h($artifact->general_comments)); ?>
    </div>
    <div>
        <?= __('CDLI Comments') ?>:
        <?= $this->Text->autoParagraph(h($artifact->cdli_comments)); ?>
    </div>

    <?php if (!empty($artifact->credits)): ?>
    <section class="related">
        <h2><?= __('Related Credits') ?></h2>
        <div>
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th scope="col"><?= __('Id') ?></th>
                        <th scope="col"><?= __('User Id') ?></th>
                        <th scope="col"><?= __('Artifact Id') ?></th>
                        <th scope="col"><?= __('Date') ?></th>
                        <th scope="col"><?= __('Comments') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($artifact->credits as $credits): ?>
                    <tr>
                        <td><?= h($credits->id) ?></td>
                        <td><?= h($credits->author_id) ?></td>
                        <td><?= h($credits->artifact_id) ?></td>
                        <td><?= h($credits->date) ?></td>
                        <td><?= h($credits->comments) ?></td>
                        <td class="actions">
                            <?= $this->Actions->manageRelation('Credits', $credits->id) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </section>
    <?php endif; ?>

    <?php if (!empty($artifact->collections)): ?>
    <section class="related">
        <h2><?= __('Related Collections') ?></h2>
        <div>
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                      <th scope="col"><?= __('Id') ?></th>
                      <th scope="col"><?= __('Collection') ?></th>
                      <th scope="col"><?= __('Geo Coordinates') ?></th>
                      <th scope="col"><?= __('Slug') ?></th>
                      <th scope="col"><?= __('Is Private') ?></th>
                      <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>

                <tbody>
                    <?php foreach ($artifact->collections as $collections): ?>
                    <tr>
                        <td><?= h($collections->id) ?></td>
                        <td><?= h($collections->collection) ?></td>
                        <td><?= h($collections->geo_coordinates) ?></td>
                        <td><?= h($collections->slug) ?></td>
                        <td><?= h($collections->is_private) ?></td>
                        <td class="actions">
                            <?= $this->Actions->manageRelation('Collections', $collections->id) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </section>
    <?php endif; ?>

    <?php if (!empty($artifact->genres)): ?>
    <section class="related">
        <h2><?= __('Related Genres') ?></h2>
        <div>
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th scope="col"><?= __('Id') ?></th>
                        <th scope="col"><?= __('Genre') ?></th>
                        <th scope="col"><?= __('Parent Id') ?></th>
                        <th scope="col"><?= __('Genre Comments') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($artifact->genres as $genres): ?>
                    <tr>
                        <td><?= h($genres->id) ?></td>
                        <td><?= h($genres->genre) ?></td>
                        <td><?= h($genres->parent_id) ?></td>
                        <td><?= h($genres->genre_comments) ?></td>
                        <td class="actions">
                            <?= $this->Actions->manageRelation('Genres', $genres->id) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </section>
    <?php endif; ?>

    <?php if (!empty($artifact->languages)): ?>
    <section class="related">
        <h2><?= __('Related Languages') ?></h2>
        <div>
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th scope="col"><?= __('Id') ?></th>
                        <th scope="col"><?= __('Order') ?></th>
                        <th scope="col"><?= __('Parent Id') ?></th>
                        <th scope="col"><?= __('Language') ?></th>
                        <th scope="col"><?= __('Protocol Code') ?></th>
                        <th scope="col"><?= __('Inline Code') ?></th>
                        <th scope="col"><?= __('Notes') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($artifact->languages as $languages): ?>
                    <tr>
                        <td><?= h($languages->id) ?></td>
                        <td><?= h($languages->order) ?></td>
                        <td><?= h($languages->parent_id) ?></td>
                        <td><?= h($languages->language) ?></td>
                        <td><?= h($languages->protocol_code) ?></td>
                        <td><?= h($languages->inline_code) ?></td>
                        <td><?= h($languages->notes) ?></td>
                        <td class="actions">
                          <?= $this->Actions->manageRelation('Languages', $languages->id) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </section>
    <?php endif; ?>

    <?php if (!empty($artifact->artifacts_composites)): ?>
    <section class="related">
        <h2><?= __('Related Artifacts Composites') ?></h2>
        <div>
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th scope="col"><?= __('Id') ?></th>
                        <th scope="col"><?= __('Composite') ?></th>
                        <th scope="col"><?= __('Artifact Id') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($artifact->artifacts_composites as $artifactsComposites): ?>
                    <tr>
                        <td><?= h($artifactsComposites->id) ?></td>
                        <td><?= h($artifactsComposites->composite) ?></td>
                        <td><?= h($artifactsComposites->artifact_id) ?></td>
                        <td class="actions">
                            <?= $this->Actions->manageRelation('ArtifactsComposites', $artifactsComposites->id) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </section>
    <?php endif; ?>

    <?php if (!empty($artifact->artifacts_date_referenced)): ?>
    <section class="related">
        <h2><?= __('Related Artifacts Date Referenced') ?></h2>
        <div>
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th scope="col"><?= __('Id') ?></th>
                        <th scope="col"><?= __('Artifact Id') ?></th>
                        <th scope="col"><?= __('Ruler Id') ?></th>
                        <th scope="col"><?= __('Month Id') ?></th>
                        <th scope="col"><?= __('Month No') ?></th>
                        <th scope="col"><?= __('Year Id') ?></th>
                        <th scope="col"><?= __('Day No') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($artifact->artifacts_date_referenced as $artifactsDateReferenced): ?>
                    <tr>
                        <td><?= h($artifactsDateReferenced->id) ?></td>
                        <td><?= h($artifactsDateReferenced->artifact_id) ?></td>
                        <td><?= h($artifactsDateReferenced->ruler_id) ?></td>
                        <td><?= h($artifactsDateReferenced->month_id) ?></td>
                        <td><?= h($artifactsDateReferenced->month_no) ?></td>
                        <td><?= h($artifactsDateReferenced->year_id) ?></td>
                        <td><?= h($artifactsDateReferenced->day_no) ?></td>
                        <td class="actions">
                            <?= $this->Actions->manageRelation('ArtifactsDateReferenced', $artifactsDateReferenced->id) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </section>
    <?php endif; ?>

    <?php if (!empty($artifact->artifacts_seals)): ?>
    <section class="related">
        <h2><?= __('Related Artifacts Seals') ?></h2>
        <div>
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th scope="col"><?= __('Id') ?></th>
                        <th scope="col"><?= __('Seal No') ?></th>
                        <th scope="col"><?= __('Artifact Id') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($artifact->artifacts_seals as $artifactsSeals): ?>
                    <tr>
                        <td><?= h($artifactsSeals->id) ?></td>
                        <td><?= h($artifactsSeals->seal_no) ?></td>
                        <td><?= h($artifactsSeals->artifact_id) ?></td>
                        <td class="actions">
                            <?= $this->Actions->manageRelation('ArtifactsSeals', $artifactsSeals->id) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </section>
    <?php endif; ?>

    <?php if (!empty($artifact->artifacts_shadow)): ?>
    <section class="related">
        <h2><?= __('Related Artifacts Shadow') ?></h2>
        <div>
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th scope="col"><?= __('Id') ?></th>
                        <th scope="col"><?= __('Artifact Id') ?></th>
                        <th scope="col"><?= __('Cdli Comments') ?></th>
                        <th scope="col"><?= __('Collection Location') ?></th>
                        <th scope="col"><?= __('Collection Comments') ?></th>
                        <th scope="col"><?= __('Acquisition History') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($artifact->artifacts_shadow as $artifactsShadow): ?>
                    <tr>
                        <td><?= h($artifactsShadow->id) ?></td>
                        <td><?= h($artifactsShadow->artifact_id) ?></td>
                        <td><?= h($artifactsShadow->cdli_comments) ?></td>
                        <td><?= h($artifactsShadow->collection_location) ?></td>
                        <td><?= h($artifactsShadow->collection_comments) ?></td>
                        <td><?= h($artifactsShadow->acquisition_history) ?></td>
                        <td class="actions">
                            <?= $this->Actions->manageRelation('ArtifactsShadow', $artifactsShadow->id) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </section>
    <?php endif; ?>

    <?php if (!empty($artifact->retired_artifacts)): ?>
    <section class="related">
        <h2><?= __('Related Retired Artifacts') ?></h2>
        <div>
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th scope="col"><?= __('Id') ?></th>
                        <th scope="col"><?= __('Artifact Id') ?></th>
                        <th scope="col"><?= __('New Artifact Id') ?></th>
                        <th scope="col"><?= __('Artifact Remarks') ?></th>
                        <th scope="col"><?= __('Is Public') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>

                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($artifact->retired_artifacts as $retiredArtifacts): ?>
                    <tr>
                        <td><?= h($retiredArtifacts->id) ?></td>
                        <td><?= h($retiredArtifacts->artifact_id) ?></td>
                        <td><?= h($retiredArtifacts->new_artifact_id) ?></td>
                        <td><?= h($retiredArtifacts->artifact_remarks) ?></td>
                        <td><?= h($retiredArtifacts->is_public) ?></td>
                        <td class="actions">
                            <?= $this->Actions->manageRelation('ArtifactsShadow', $retiredArtifacts->id) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </section>
    <?php endif; ?>
</main>
