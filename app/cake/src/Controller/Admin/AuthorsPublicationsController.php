<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class AuthorsPublicationsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'order' => [
                'author_id' => 'ASC',
                'publication_id' => 'ASC'
            ],
            'contain' => ['Publications', 'Authors']
        ];
        $authorsPublications = $this->paginate($this->AuthorsPublications);

        $this->set(compact('authorsPublications'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($flag = 0, $id = null)
    {
        if ($flag < 0 or $flag > 2 or ($flag != 0 and ($id == null or $id < 1))) {
            throw new NotFoundException('Invalid URL');
        }

        $this->loadModel('EditorsPublications');
        if ($flag == 0) {
            $authors_list = $this->AuthorsPublications->Authors->find('all');
            $authors_mapping = [];
            foreach ($authors_list as $row) {
                $authors_mapping[$row['author']] = $row['id'];
            }
            $authors_names = array_keys($authors_mapping);
            $authorsPublication = $this->AuthorsPublications->newEntity();
            if ($this->request->is('post')) {
                $data = $this->request->getData();
                if (in_array($data['author_id'], $authors_names)) {
                    $data['author_id'] = $authors_mapping[$data['author_id']];
                } else {
                    $data['author_id'] = 0;
                }
                $authorsPublication = $this->AuthorsPublications->patchEntity($authorsPublication, $data);
                if ($this->AuthorsPublications->save($authorsPublication)) {
                    $this->Flash->success(__('New link has been saved.'));
                    return $this->redirect(['action' => 'add']);
                } else {
                    $this->Flash->error(__('The link could not be saved. Please, try again.'));
                }
            }
            $this->set(compact('authorsPublication', 'authors_names', 'flag'));
        } elseif ($flag == 1) {
            $this->paginate = [
                'AuthorsPublications' => ['limit' => 10, 'scope' => 'authorspublications'],
                'EditorsPublications' => ['limit' => 10, 'scope' => 'editorspublications']
            ];
            $authorsPublication = $this->AuthorsPublications->newEntity();
            $editorsPublication = $this->EditorsPublications->newEntity();
            $authors_list = $this->AuthorsPublications->Authors->find('all');
            $authors_mapping = [];
            foreach ($authors_list as $row) {
                $authors_mapping[$row['author']] = $row['id'];
            }
            $authors_names = array_keys($authors_mapping);
            if ($this->request->is('post')) {
                $data = $this->request->getData();
                if (array_key_exists('author_id', $data)) {
                    // For adding author publication link
                    if (in_array($data['author_id'], $authors_names)) {
                        $data['author_id'] = $authors_mapping[$data['author_id']];
                    } else {
                        $data['author_id'] = 0;
                    }
                    $authorsPublication = $this->AuthorsPublications->patchEntity($authorsPublication, $data);
                    if ($this->AuthorsPublications->save($authorsPublication)) {
                        $this->Flash->success(__('The author has been linked.'));
                        return $this->redirect(['action' => 'add', 1, $id]);
                    }
                    $this->Flash->error(__('The author could not be linked. Please, try again.'));
                } else {
                    // For adding editor publication link
                    if (in_array($data['editor_id'], $authors_names)) {
                        $data['editor_id'] = $authors_mapping[$data['editor_id']];
                    } else {
                        $data['editor_id'] = 0;
                    }
                    $editorsPublication = $this->EditorsPublications->patchEntity($editorsPublication, $data);
                    if ($this->EditorsPublications->save($editorsPublication)) {
                        $this->Flash->success(__('The editor has been linked.'));
                        return $this->redirect(['action' => 'add', 1, $id]);
                    }
                    $this->Flash->error(__('The editor could not be linked. Please, try again.'));
                }
            }
            $authorsPublications = $this->paginate($this->AuthorsPublications->find('all', ['order' => 'author_id', 'contain' => ['Authors', 'Publications']])->where(['publication_id' => $id]));
            $editorsPublications = $this->paginate($this->EditorsPublications->find('all', ['order' => 'editor_id', 'contain' => ['Authors', 'Publications']])->where(['publication_id' => $id]));
            $this->set(compact('id', 'authorsPublications', 'authorsPublication', 'editorsPublications', 'editorsPublication', 'authors_names', 'flag'));
        } elseif ($flag == 2) {
            $this->paginate = [
                'AuthorsPublications' => ['limit' => 10, 'scope' => 'authorspublications'],
                'EditorsPublications' => ['limit' => 10, 'scope' => 'editorspublications']
            ];
            $authorsPublication = $this->AuthorsPublications->newEntity();
            $editorsPublication = $this->EditorsPublications->newEntity();
            if ($this->request->is('post')) {
                $data = $this->request->getData();
                if (array_key_exists('author_id', $data)) {
                    // For adding author publication link
                    $authorsPublication = $this->AuthorsPublications->patchEntity($authorsPublication, $data);
                    if ($this->AuthorsPublications->save($authorsPublication)) {
                        $this->Flash->success(__('The author has been linked.'));
                        return $this->redirect(['action' => 'add', 2, $id]);
                    }
                    $this->Flash->error(__('The publication could not be linked. Please, try again.'));
                } else {
                    // For adding editor publication link
                    $editorsPublication = $this->EditorsPublications->patchEntity($editorsPublication, $data);
                    if ($this->EditorsPublications->save($editorsPublication)) {
                        $this->Flash->success(__('The editor has been linked.'));
                        return $this->redirect(['action' => 'add', 2, $id]);
                    }
                    $this->Flash->error(__('The publication could not be linked. Please, try again.'));
                }
            }
            $authorsPublications = $this->paginate($this->AuthorsPublications->find('all', ['order' => 'publication_id', 'contain' => ['Authors', 'Publications']])->where(['author_id' => $id]));
            $editorsPublications = $this->paginate($this->EditorsPublications->find('all', ['order' => 'publication_id', 'contain' => ['Authors', 'Publications']])->where(['editor_id' => $id]));
            $this->set(compact('id', 'authorsPublications', 'authorsPublication', 'editorsPublications', 'editorsPublication', 'flag'));
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Authors Publication id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null, $flag = 0, $parent_id = null)
    {
        if ($id < 1 or $flag < 0 or $flag > 2 or ($flag != 0 and ($parent_id == null or $parent_id < 1))) {
            throw new NotFoundException('Invalid URL');
        }

        $authorsPublication = $this->AuthorsPublications->get($id, [
                        'contain' => ['Publications', 'Authors']
                    ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $authorsPublication = $this->AuthorsPublications->patchEntity($authorsPublication, $this->request->getData());
            if ($this->AuthorsPublications->save($authorsPublication)) {
                $this->Flash->success(__('Changes has been saved.'));

                if ($flag == 0) {
                    return $this->redirect(['action' => 'index']);
                } elseif ($flag == 1) {
                    return $this->redirect(['action' => 'add', 1, $parent_id]);
                } elseif ($flag == 2) {
                    return $this->redirect(['action' => 'add', 2, $parent_id]);
                }
            }
            $this->Flash->error(__('Changes could not be saved. Please, try again.'));
        }
        $authorsPublications = $this->AuthorsPublications->find('all', ['contain' => ['Authors', 'Publications']])->where(['publication_id' => $authorsPublication->publication_id])->all();
        $this->set(compact('authorsPublication', 'flag', 'parent_id', 'authorsPublications'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Authors Publication id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $flag = 0, $parent_id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $authorsPublication = $this->AuthorsPublications->get($id);
        if ($this->AuthorsPublications->delete($authorsPublication)) {
            $this->Flash->success(__('The link has been deleted.'));
        } else {
            $this->Flash->error(__('The link could not be deleted. Please, try again.'));
        }

        if ($flag == 0) {
            return $this->redirect(['action' => 'index']);
        } elseif ($flag == 1) {
            return $this->redirect(['action' => 'add', 1, $parent_id]);
        } elseif ($flag == 2) {
            return $this->redirect(['action' => 'add', 2, $parent_id]);
        }
    }
}
