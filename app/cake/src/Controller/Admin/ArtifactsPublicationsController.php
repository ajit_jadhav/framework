<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

/**
 * ArtifactsPublications Controller
 *
 * @property \App\Model\Table\ArtifactsPublicationsTable $ArtifactsPublications
 *
 * @method \App\Model\Entity\ArtifactsPublication[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactsPublicationsController extends AppController
{
    /**
     * Index method.
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'order' => [
                'artifact_id' => 'ASC',
                'publication_id' => 'ASC'
            ],
            'contain' => [
                'Artifacts', 'Publications'
            ]
        ];
        $artifactsPublications = $this->paginate($this->ArtifactsPublications);

        $this->set(compact('artifactsPublications'));
    }

    /**
     * Add method.
     *
     * @param string $flag type of add operation.
     * '' => Normal add,
     * 'artifact' => Add link to a selected publication,
     * 'publication' => Add link to a selected artifact,
     * 'bulk' => Bulk add links.
     * @param int $id Id of the selected publication or selected artifact for $flag = 'artifact' and $flag = 'publication' respectively.
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($flag = '', $id = null)
    {
        if ($flag == '') {
            $artifactsPublication = $this->ArtifactsPublications->newEntity();
            if ($this->request->is('post')) {
                $artifactsPublication = $this->ArtifactsPublications->patchEntity($artifactsPublication, $this->request->getData());
                if ($this->ArtifactsPublications->save($artifactsPublication)) {
                    $this->Flash->success('New link has been saved.');
                    return $this->redirect(['action' => 'add']);
                }
                $this->Flash->error('New link could not be saved. Please, try again.');
            }
            $this->set(compact('artifactsPublication', 'flag'));
        } elseif ($flag == 'artifact') {
            $this->paginate = [
                'limit' => 10
            ];
            $artifactsPublication = $this->ArtifactsPublications->newEntity();
            
            if ($this->request->is('post')) {
                $artifactsPublication = $this->ArtifactsPublications->patchEntity($artifactsPublication, $this->request->getData());
                if ($this->ArtifactsPublications->save($artifactsPublication)) {
                    $this->Flash->success(__('The artifact has been linked.'));
                    return $this->redirect(['action' => 'add', $flag, $id]);
                }
                $this->Flash->error(__('The artifact could not be linked. Please, try again.'));
            }
            $artifactsPublications = $this->paginate($this->ArtifactsPublications->find('all', ['order' => 'artifact_id', 'contain' => ['Artifacts', 'Publications']])->where(['publication_id' => $id]));
            $this->set(compact('id', 'artifactsPublications', 'artifactsPublication', 'flag'));
        } elseif ($flag == 'publication') {
            $this->paginate = [
                'limit' => 10
            ];
            $artifactsPublication = $this->ArtifactsPublications->newEntity();
            if ($this->request->is('post')) {
                $artifactsPublication = $this->ArtifactsPublications->patchEntity($artifactsPublication, $this->request->getData());
                if ($this->ArtifactsPublications->save($artifactsPublication)) {
                    $this->Flash->success(__('The publication has been linked.'));
                    return $this->redirect(['action' => 'add', $flag, $id]);
                }
                $this->Flash->error(__('The publication could not be linked. Please, try again.'));
            }
            $artifactsPublications = $this->paginate($this->ArtifactsPublications->find('all', ['order' => 'publication_id', 'contain' => ['Artifacts', 'Publications']])->where(['artifact_id' => $id]));
            $this->set(compact('id', 'artifactsPublications', 'artifactsPublication', 'flag'));
        } elseif ($flag == 'bulk') {
            $this->loadComponent('BulkUpload', ['table' => 'ArtifactsPublications']);
            if ($this->request->is('post')) {
                $data = $this->request->data['csv'];
                list($file_error, $all_rows, $raw_data, $header) = $this->BulkUpload->load($data);
                if ($file_error == []) {
                    list($error_list, $entities) = $this->BulkUpload->validate($all_rows, $raw_data);
                    if ($error_list == []) {
                        $error_list = null;

                        $this->BulkUpload->save($entities);
                        $this->Flash->success(__('All entries have been successfully saved.'));
                    } else {
                        $this->Flash->error(__('Some of the entries cannot be saved due to errors. Do you still want to proceed with saving the entries without errors?'));
                    }
                } else {
                    foreach ($file_error as $error) {
                        $this->Flash->error(__($error));
                    }
                }
            } elseif (isset($this->request->getAttribute('params')['?'])) {
                $error_list = unserialize($this->request->getAttribute('params')['?']['error_list_serialize']);
                $header = unserialize($this->request->getAttribute('params')['?']['header_serialize']);

                $this->BulkUpload->save(unserialize($this->request->getAttribute('params')['?']['entities_serialize']));
                $this->Flash->success(__('All entries without errors have been successfully saved.'));
            }

            $this->set(compact('flag', 'error_list', 'header', 'entities'));
        }
    }

    /**
     * Edit method.
     *
     * @param int $id Artifact Publication Link id.
     * @param int $flag page to redirect to.
     * '' => Normal edit,
     * 'artifact' => Edit page for a selected publication,
     * 'publication' => Edit page for a selected artifact.
     * @param int $parent_id Id of the selected publication or selected artifact for $flag = 1 and $flag = 2 respectively.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null, $flag = '', $parent_id = null)
    {
        $artifactsPublication = $this->ArtifactsPublications->get($id, [
            'contain' => ['Artifacts', 'Publications']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $artifactsPublication = $this->ArtifactsPublications->patchEntity($artifactsPublication, $this->request->getData());
            if ($this->ArtifactsPublications->save($artifactsPublication)) {
                $this->Flash->success(__('Changes has been saved.'));

                if ($flag == '') {
                    return $this->redirect(['action' => 'index']);
                } else {
                    return $this->redirect(['action' => 'add', $flag, $parent_id]);
                }
            }
            $this->Flash->error(__('Changes could not be saved. Please, try again.'));
        }
        $this->set(compact('artifactsPublication', 'flag', 'parent_id'));
    }

    /**
     * Delete method.
     *
     * @param int $id Artifact Publication Link id.
     * @param int $flag page to redirect to.
     * '' => Index page,
     * 'artifact' => Add page for a selected publication,
     * 'publication' => Add page for a selected artifact.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $flag = '', $parent_id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $artifactsPublication = $this->ArtifactsPublications->get($id);
        if ($this->ArtifactsPublications->delete($artifactsPublication)) {
            $this->Flash->success(__('The link has been deleted.'));
        } else {
            $this->Flash->error(__('The link could not be deleted. Please, try again.'));
        }

        if ($flag == '') {
            return $this->redirect(['action' => 'index']);
        } else {
            return $this->redirect(['action' => 'add', $flag, $parent_id]);
        }
    }

    /**
     * Export method for downloading the entries containing errors.
     *
     * @param array $error_list_serialize Serialized list of all the objects containing errors.
     * @param array $header_serialize Serialized list of headers for the file.
     */
    public function export($error_list_serialize, $header_serialize)
    {
        $this->loadComponent('BulkUpload', ['table' => 'ArtifactsPublications']);
        list($data, $_serialize, $_header) = $this->BulkUpload->export($error_list_serialize, $header_serialize);
        
        $this->set(compact('data', '_serialize', '_header'));
        $this->viewBuilder()->className('CsvView.Csv');
        return;
    }
}
