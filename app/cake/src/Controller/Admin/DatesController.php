<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Dates Controller
 *
 * @property \App\Model\Table\DatesTable $Dates
 *
 * @method \App\Model\Entity\Date[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DatesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Months', 'Years', 'Dynasties', 'Rulers']
        ];
        $dates = $this->paginate($this->Dates);

        $this->set(compact('dates'));
    }

    /**
     * View method
     *
     * @param string|null $id Date id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $date = $this->Dates->get($id, [
            'contain' => ['Months', 'Years', 'Dynasties', 'Rulers', 'Artifacts']
        ]);

        $this->set('date', $date);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $date = $this->Dates->newEntity();
        if ($this->request->is('post')) {
            $date = $this->Dates->patchEntity($date, $this->request->getData());
            if ($this->Dates->save($date)) {
                $this->Flash->success(__('The date has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The date could not be saved. Please, try again.'));
        }
        $months = $this->Dates->Months->find('list', ['limit' => 200]);
        $years = $this->Dates->Years->find('list', ['limit' => 200]);
        $dynasties = $this->Dates->Dynasties->find('list', ['limit' => 200]);
        $rulers = $this->Dates->Rulers->find('list', ['limit' => 200]);
        $artifacts = $this->Dates->Artifacts->find('list', ['limit' => 200]);
        $this->set(compact('date', 'months', 'years', 'dynasties', 'rulers', 'artifacts'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Date id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $date = $this->Dates->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $date = $this->Dates->patchEntity($date, $this->request->getData());
            if ($this->Dates->save($date)) {
                $this->Flash->success(__('The date has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The date could not be saved. Please, try again.'));
        }
        $months = $this->Dates->Months->find('list', ['limit' => 200]);
        $years = $this->Dates->Years->find('list', ['limit' => 200]);
        $dynasties = $this->Dates->Dynasties->find('list', ['limit' => 200]);
        $rulers = $this->Dates->Rulers->find('list', ['limit' => 200]);
        $artifacts = $this->Dates->Artifacts->find('list', ['limit' => 200]);
        $this->set(compact('date', 'months', 'years', 'dynasties', 'rulers', 'artifacts'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Date id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $date = $this->Dates->get($id);
        if ($this->Dates->delete($date)) {
            $this->Flash->success(__('The date has been deleted.'));
        } else {
            $this->Flash->error(__('The date could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
