<?php
namespace App\Controller;

use App\Controller\AppController;

class HomeController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */

    public function index()
    {
        $this->loadModel('Postings');
        $this->set('newsarticles', $this->Postings->find('all', [
              'conditions' => ['Postings.published' => 1, 'posting_type_id' => 1],
              'limit' => 6
            ]));
        $this->set('highlights', $this->Postings->find('all', [
            'conditions' => ['Postings.published' => 1, 'posting_type_id' => 3],
            'limit' => 6
            ]));
    }
}
