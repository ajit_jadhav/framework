<?php
namespace App\Controller;

use App\Controller\AppController;
use GoogleAuthenticator\GoogleAuthenticator;
use Cake\I18n\Time;

/**
 * TwofactorController Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TwofactorController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['index']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Authors', 'Credits', 'Inscriptions']
        ]);

        $this->set('user', $user);
    }

    /**
     * Index method (2FA Setup Method)
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // Get Session
        $session = $this->getRequest()->getSession();

        // Check if 2FA code has been verified or 2FA is set up for that particular session is.
        $session_verified_exists = ($session->read('session_verified') != null) ? true : false;
        
        // Check if session contains 'user' details
        $session_user_status = ($session->read('user') != null) ? true : false;

        // If session 'user' is present then execute next logic. If not then redirect to '/'
        if ($session_user_status) {
            $session_user = $session->read('user');

            // If present session is verified then redirect to '/'
            if ($session_verified_exists) {
                return $this->redirect([
                    "controller" => "Search",
                    "action" => "index"
                ]);
            } else {
                // Check if 2FA Enabled or not. If enabled redirect to verify page or redirect to 2FA setup page.
                if ($session_user['2fa_status']) {
                    $this->setAction("twofaverify");
                } else {
                    $this->loadModel('Users');
                    $this->loadComponent('GoogleAuthenticator');
                    $ga = $this->GoogleAuthenticator;

                    if ($this->request->is('post')) {
                        $checkconfirm = $this->request->data['checkconfirm'];

                        if ($checkconfirm == 0) {
                            $this->Flash->error(__('Please back up your 16-digit key before proceeding.'), ['class' => 'alert alert-danger']);

                            return $this->redirect([
                                "controller" => "Twofactor",
                                "action" => "index"
                            ]);
                        }

                        $secret = $this->request->data['secretcode'];
                        $oneCode = $this->request->data['code'];
                        $checkResult = $ga->verifyCode($secret, $oneCode, 2);    // 2 = 2*30sec clock tolerance

                        if ($checkResult) {
                            $time_now = Time::now();
                            
                            // If requested from Register then Session contains user's email
                            if ($session->read('user.email') == null) {
                                $updateUser['2fa_key'] =  $this->request->data['secretcode'];
                                $updateUser['2fa_status'] = 1;
                                $updateUser['last_login_at'] = $time_now;
                                $updateUser['modified_at'] = $time_now;

                                $retrivedUser = $this->Users->find('all', [
                                    'conditions' => [
                                        'Users.username' => $session_user['username']
                                    ]
                                ])->first();

                                $saveUser = $this->Users->patchEntity($retrivedUser, $updateUser);
                            } else {
                                unset($session_user['created']);
                                $modifiedData['email'] = $session_user['email'];
                                $modifiedData['username'] = $session_user['username'];
                                $modifiedData['2fa_key'] =  $this->request->data['secretcode'];
                                $modifiedData['2fa_status'] = 1;
                                $modifiedData['last_login_at'] = $time_now;
                                $modifiedData['created_at'] = $time_now;
                                $modifiedData['modified_at'] = $time_now;
                                
                                $saveUser = $this->Users->patchEntity($session_user, $modifiedData);
                                
                                if (!empty($saveUser->errors())) {
                                    foreach ($saveUser->errors() as $error) {
                                        foreach ($error as $key => $value) {
                                            $this->Flash->error($value);
                                        }
                                    }
                                    
                                    $session->delete('user');
                                    
                                    return $this->redirect([
                                        'controller' => 'Register',
                                        'action' => 'index'
                                    ]);
                                }
                            }
                            
                            if ($this->Users->save($saveUser)) {
                                if ($session->read('user.email') == null) {
                                    // When User Logs in
                                    $this->AuthenticateUsersRole($session_user['username']);
                                } else {
                                    // When User Registers
                                    $this->AuthenticateUsersRole($session_user['username'], 1);
                                }

                                $session->delete('user');
                                
                                $this->Flash->success(__('Two-Factor Authentication (2FA) Is Enabled.'), ['class' => 'alert alert-danger']);
                                
                                return $this->redirect([
                                    "controller" => "Home",
                                    "action" => "index"
                                ]);
                            } else {
                                return $this->Flash->error(__('Please try again.'), ['class' => 'alert alert-danger']);
                            }
                        } else {
                            return $this->Flash->error(__('Wrong code entered.Please try again.'), ['class' => 'alert alert-danger']);
                        }
                    }
                }
            }
        } else {
            return $this->redirect([
                "controller" => "Home",
                "action" => "index"
            ]);
        }
    }

    /**
     * 2FA Verify method
     *
     * @return \Cake\Http\Response|void
     */
    public function twofaverify()
    {
        $this->loadComponent('GoogleAuthenticator');
        $this->loadModel('Users');
        $ga = $this->GoogleAuthenticator;
        $user = $this->getRequest()->getSession()->read('user');

        if ($this->request->is('post')) {
            $secret = $this->get2faKey($user['username']);
            $oneCode = $this->request->data['code'];
            $checkResult = $ga->verifyCode($secret, $oneCode, 2);

            if ($checkResult) {
                $updateUser['last_login_at'] = Time::now();

                $retrivedUser = $this->getUser($user['username']);

                $saveUser = $this->Users->patchEntity($retrivedUser, $updateUser);

                if (empty($saveUser->errors()) && $this->Users->save($saveUser)) {
                    $this->AuthenticateUsersRole($user['username']);
                    $this->getRequest()->getSession()->delete('user');
                    
                    return $this->redirect([
                        'controller' => 'Home',
                        'action' => 'index'
                    ]);
                } else {
                    $this->getRequest()->getSession()->delete('user');
                    $this->Flash->error(__("Error on Server Side !!"));
                    return $this->redirect(['controller' => 'Home', 'action' => 'index']);
                }
            } else {
                $this->Flash->error(__('Wrong code entered.Please try again.'), [
                    'class' => 'alert alert-danger'
                ]);
            }
        }
    }

    // Retrive user from DB
    public function getUser($username)
    {
        $this->loadModel('Users');
        $user = $this->Users->find(
            'all',
            [
            'conditions' => [
                'Users.username' => $username]
            ]
        )->first();
        return $user;
    }

    // Authenticates User's Login or Registers.
    public function AuthenticateUser($user, $roles)
    {
        $this->getRequest()->getSession()->write('session_verified', 1);

        $authUser['id'] = $user['id'];
        $authUser['username'] = $user['username'];
        $authUser['email'] = $user['email'];
        $authUser['author_id'] = $user['author_id'];
        $authUser['last_login_at'] = $user['last_login_at'];
        $authUser['active'] = $user['active'];
        $authUser['modified_at'] = $user['modified_at'];
        $authUser['hd_images_collection_id'] = $user['hd_images_collection_id'];
        $authUser['roles'] = $roles;

        $this->Auth->setUser($authUser);
    }

    // Returns 2FA key for specific username
    public function get2faKey($username)
    {
        $user = $this->getUser($username);
        return $user['2fa_key'];
    }

    /**
     * Set/Get User's Role method on successful GET, Authenticate User
     *
     * SET Role (Registration) and GET Role (Registration , Login)
     *
     * @param : $username, $type
     * username -> Username
     * type -> 1 for SET
     *
     * @return : void
     */
    public function AuthenticateUsersRole($username, $type = null)
    {
        $this->loadModel('RolesUsers');

        $user = $this->getUser($username);

        // SET User Role
        if ($type == 1) {
            // By default setting it to Role_Id : 10 (Normal User)
            $roles = $this->setUsersRole($user['id'], [10], null);
        } else {
            // GET User Role
            $roles = $this->getUsersRole($user['id']);
        }
        $this->AuthenticateUser($user, $roles);
    }

    /**
     * User Role
     *
     * @param : $userId
     * userId -> User ID
     *
     * @return : array of roles
     */
    public function getUsersRole($userId)
    {
        $this->loadModel('RolesUsers');

        // GET User Role
        $rolesQuery = $this->RolesUsers->findByUserId($userId);

        $roles = [];

        foreach ($rolesQuery as $roleUser => $individualRole) {
            array_push($roles, $individualRole['role_id']);
        }
        return $roles;
    }

    /**
     * Set Roles
     *
     * @param : $userId, $toBeSet, $toBeRemoved
     * userId -> User ID
     * toBeSet -> Roles to be added (array)
     * toBeRemoved -> Roles to be removed (array)
     *
     * @return : array of roles
     */
    public function setUsersRole($userId, $toBeSet = null, $toBeRemoved = null)
    {
        $this->loadModel('RolesUsers');

        if (!is_null($toBeRemoved)) {
            if (!$this->RolesUsers->deleteAll(['user_id' => $userId, 'role_id IN' => $toBeRemoved])) {
                $this->Flash->error(__("Error in setting Roles !!"));
            };
        }

        if (!is_null($toBeSet)) {
            $userRole = [];

            foreach ($toBeSet as $role_id) {
                array_push($userRole, ['user_id' => $userId, 'role_id' => $role_id]);
            }
            
            if (!$this->RolesUsers->saveMany($this->RolesUsers->newEntities($userRole))) {
                $this->Flash->error(__("Error in setting Roles !!"));
            }
        }

        return $this->getUsersRole($userId);
    }
}
