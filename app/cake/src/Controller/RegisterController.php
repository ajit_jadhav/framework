<?php
namespace App\Controller;

use App\Controller\AppController;
use GoogleAuthenticator\GoogleAuthenticator;
use Cake\I18n\Time;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RegisterController extends AppController
{

    /**
     * Load Auth component
     */
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['index']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        // If already login
        if ($this->Auth->user()) {
            // Redirected to Profile (/users/{id}) through User Controller.
            return $this->redirect([
                'controller' => 'Users',
                'action' => 'index',
            ]);
        }

        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $newUser =  $this->validateUser($data);
            $errors = $newUser->errors();
            $badpasswordStatus = $this->checkBadPasswords($newUser, $data["password"]);
            $errorStatus = !empty($errors) || $badpasswordStatus ;
            
            // Display errors generated after validation of user's data.
            if ($errorStatus) {
                if (!empty($errors)) {
                    $this->set("errors", $errors);
                }
                if ($badpasswordStatus) {
                    $this->set("badPassword", $badpasswordStatus);
                }
            } else {
                $session = $this->getRequest()->getSession();
                $modified_user_data = $newUser;
                $modified_user_data['created'] = Time::now();

                // Storing in session variable 'user' : [username, 2fa_key, 2fa_status, created]
                $session->write('user', $modified_user_data);

                // This session_verified = 'false' will be set for every new session. Will be set 'true' once user submit 2FA secret code or setup 2FA for first time.
                $this->getRequest()->getSession()->write('session_verified', 0);

                return $this->redirect([
                    'controller' => 'Twofactor',
                    'action' => 'index',
                ]);
            }
        }
    }

    // Validates User data using Models validation Rules
    public function validateUser($userData)
    {
        $this->loadModel('Users');
        $newUser = $this->Users->newEntity($userData);
        return $newUser;
    }

    // Check input passwords with list of bad passwords.
    public function checkBadPasswords($user, $password)
    {
        $file = "badpassword.txt";
        $file = file_get_contents($file);
        $file = explode("\n", $file);
        $email = explode('@', $user['email'])[0];
        $username = $user['username'];

        if ($password === $email || $password === $username) {
            return 1;
        }
        return in_array($password, $file);
    }
}
